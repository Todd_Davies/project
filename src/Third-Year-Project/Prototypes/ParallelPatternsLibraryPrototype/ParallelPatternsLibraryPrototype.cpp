// Computes the collatz conjuncure for a set of numbers in parallel

#include "stdafx.h"

#include <ppl.h>
#include <Windows.h>
#include <vector>
#include <concurrent_vector.h>

using namespace concurrency;
using namespace std;

template <class Function>
__int64 time_call(Function&& f)
{
  __int64 begin = GetTickCount();
  f();
  return GetTickCount() - begin;
}

int collatz(int input) {
  int i = 0;
  while (input != 1) {
    if (input % 2 == 0) input /= 2;
    else input = (input * 3) + 1;
    if (input < 0) break;
    else i++;
  }
  return i;
}

const int NUM = 50000000;

int _tmain(int argc, _TCHAR* argv[])
{
  cout << "Serial: ";
  __int64 elapsed;
  vector<int> *serArr = new vector<int>;
  elapsed = time_call([&] {
    for (int i = 1; i < NUM; i++) {
      serArr->push_back(collatz(i));
    }
  });
  cout << elapsed << "ms\n";
  delete serArr;

  cout << "Parallel: ";
  concurrent_vector<int> *parArr = new concurrent_vector<int>();
  elapsed = time_call([&] {
    parallel_for(1, NUM, [&](int i) {
      parArr->push_back(collatz(i));
    });
  });
  cout << elapsed << "ms\n";
  delete parArr;
}

