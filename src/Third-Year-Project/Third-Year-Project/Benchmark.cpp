#include "stdafx.h"
#include "Demo.h"

TagFrequency* trainBaseTagger(BncIterator* bnc, int sentences, bool quiet) {
  // Train the base tagger
  TagFrequency *base = new TagFrequency();
  int i = 0;
  for (int i = 0; i < sentences&& !bnc->isEmpty();) {
    for (TrainingWord t : ***bnc) base->add(t);
    bnc++;
    i++;
    if (quiet && i % 1000 == 0) {
      cout << "\rTraining base tagger: " << (100.0 / sentences) * i << "% ";
    }
  }
  return base;
}

void runPerformanceBenchmark(char* bncPath, char* templatePath, int numBaseTrain,
    int numBrillTrain, bool parallel, int quiet) {
  // Keep track of the tags and words we've seen
  concurrent_unordered_set<char*, CharHasher, CharEquals> tags, words;
  // Create a BNC iterator
  BncIterator bnc = BncIterator(bncPath, &tags, &words, true);
  // Train a base tagger
  TagFrequency *base = trainBaseTagger(&bnc, numBaseTrain, quiet == 0);
  // Load the rule templates
  if (!quiet) cout << "\nParsing rule templates... ";
  Parser p;
  vector<Rule*> ruleTemplates = p.parseTemplateFile(templatePath);
  if (!quiet) cout << ruleTemplates.size() << " templates parsed\n";
  // Get number of cores
  int cpuNum = std::thread::hardware_concurrency();
  // Train the rules
  cout << (parallel ? "PARALLEL" : "SERIAL");
  if (parallel) cout << cpuNum;
  cout << "\n";
  int numRules = 0;
  vector<Rule*> instansiatedRules = (parallel)
    ? trainRulesParallel(ruleTemplates, &bnc, base, &tags, &words, numBrillTrain, 1, !quiet, 5, &numRules)
    : trainRules(ruleTemplates, &bnc, base, &tags, &words, numBrillTrain, 1, !quiet, 5, &numRules);
  // Deallocate the templates
  for (Rule* r : ruleTemplates) delete r;
  delete base;
}