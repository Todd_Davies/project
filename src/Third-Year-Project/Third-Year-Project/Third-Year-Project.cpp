// Third-Year-Project.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace brill_tagger;
using namespace bnc_parser;

int NUM_BASE_TRAIN = 0;
int NUM_BRILL_TRAIN = 74999;
int SENTENCE_LENGTH;

char* templatePath = "C:\\temp\\BrillRulesTest.tmpl";

char* bncPath;
bool parallel;
int quiet;

void usage() {
  cout << "ThirdYearProject.exe <training_setntences> <sentence_length> <parallel> <corpus_path> <quiet> <demo_mode>\n";
}

int main(int argc, char* argv[])
{
  // Check we have a valid number of arguments
  if (argc != 7) {
    usage();
    return -1;
  }
  // Parse the args
  SENTENCE_LENGTH = stoi(argv[2]);
  NUM_BRILL_TRAIN = stoi(argv[1]);
  parallel = strcmp("true", argv[3]) == 0;
  bncPath = argv[4];
  quiet = stoi(argv[5]);
  bool demo = strcmp("true", argv[6]) == 0;
  // Run the demo or the performance benchmark
  if (demo){
    runDemo(bncPath, templatePath, NUM_BRILL_TRAIN, parallel);
  } else {
    runPerformanceBenchmark(bncPath, templatePath, NUM_BASE_TRAIN, NUM_BRILL_TRAIN, parallel, quiet);
  }
}