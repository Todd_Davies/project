#include "stdafx.h"

using namespace brill_tagger;
using namespace bnc_parser;

void runPerformanceBenchmark(char* bncPath, char* templatePath, int numBaseTrain,
  int numBrillTrain, bool parallel, int quiet);