#include "stdafx.h"
#include "Demo.h"

void runDemo(char* bncPath, char* templateFile, int chunkSize, bool parallel) {
   // Random numbers
   std::random_device rd;
   std::mt19937 rng(rd());
   std::uniform_int_distribution<int> uni(0, 100000);
   // Parse the rule templates
   printf("Parsing rule templates; ");
   Parser p;
   vector<Rule*> ruleTemplates = p.parseTemplateFile("C:\\temp\\BrillRulesTest.tmpl");
   printf("%d templates parsed\n", ruleTemplates.size());
   // Base tagger
   TagFrequency *base = new TagFrequency();
   // Set up variables
   printf("Operating on chunks of %d words\n", chunkSize);
   printf("Processing data %s:\n", parallel ? "in parallel" : "serially");
   // Repeat forever
   while (true) {
     // Create a bnc corpus
     concurrent_unordered_set<char*, CharHasher, CharEquals> tags, words;
     BncIterator bnc = BncIterator(bncPath, &tags, &words, true);
     // Move to a random index
     for (int i = 0; i < uni(rng); i++) bnc++;
     // Get the start time
     auto start = std::chrono::high_resolution_clock::now();
     std::chrono::milliseconds ms(0);
     int numRules = 0;
     // Repeatedly score the corpus for one second
     while (ms.count() < 1000) {
       vector<Rule*> rules = (parallel)
         ? trainRulesParallel(ruleTemplates, &bnc, base, &tags, &words, chunkSize, 1, 0, 5, &numRules)
         : trainRules(ruleTemplates, &bnc, base, &tags, &words, chunkSize, 1, 0, 5, &numRules);
       auto finish = std::chrono::high_resolution_clock::now();
       ms = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
     }
     double seconds = ms.count() / 1000.0;
     // Print rules per second
     printf("\rRules scored/second = %.3f", (float)numRules / seconds);
   }
}