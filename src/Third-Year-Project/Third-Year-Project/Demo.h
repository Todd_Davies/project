#include "stdafx.h"

using namespace std;
using namespace brill_tagger;
using namespace bnc_parser;

void runDemo(char* bncPath, char* templateFile, int chunkSize, bool parallel);