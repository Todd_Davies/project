#ifndef PROJ_DEFINITIONS
#define PROJ_DEFINITIONS

typedef struct { int word; int tag; } WordTag;
typedef struct BRule {
  // true = tag
  bool t1, t2, t3;
  // index of the value to compare with
  int i1, i2, i3;
  // value (negative = no rule)
  int v1, v2, v3;
  // The new value
  int vx;

  bool operator==(const BRule &other) const
  {
    return (t1 == other.t1 && t2 == other.t2 && t3 == other.t3)
      && (v1 == other.v1 && v2 == other.v2 && v3 == other.v3)
      && (i1 == other.i1 && i2 == other.i2 && i3 == other.i3)
      && vx == other.vx;
  }
} BrillRule;

#include <fstream>
struct BrillRuleHasher
{
  size_t operator()(const BrillRule& t) const {
    size_t result = 7;
    result = (37 * result) + t.t1;
    result = (37 * result) + t.t2;
    result = (37 * result) + t.t3;
    result = (37 * result) + t.i1;
    result = (37 * result) + t.i2;
    result = (37 * result) + t.i3;
    result = (37 * result) + t.v1;
    result = (37 * result) + t.v2;
    result = (37 * result) + t.v3;
    result = (37 * result) + t.vx;
    return result;
  }
};

typedef struct {
  // Words & Tags
  WordTag* content;
  // Sentence start positions
  int* positions;
  // Number of sentences
  int numSentences;
  // Number of words
  int numWords;
} Corpus;

const WordTag sent_end = { -1, -1 };

#include <iostream>
static void printRule(BrillRule* rule) {
  if (rule->v1 >= -1) {
    std::cout << (rule->t1 ? "TAG[" : "WORD[");
    std::cout << rule->i1 << "]==" << rule->v1;
  }
  if (rule->v2 >= -1) {
    std::cout << (rule->t2 ? ",TAG[" : "WORD[");
    std::cout << rule->i2 << "]==" << rule->v2;
  }
  if (rule->v3 >= -1) {
    std::cout << (rule->t3 ? ",TAG[" : "WORD[");
    std::cout << rule->i3 << "]==" << rule->v3;
  }
  std::cout << " = " << rule->vx << "\n";
}

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
  if (code != cudaSuccess)
  {
    std::fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
  }
}

__device__ static bool doesApply(WordTag* corpus, int index, BrillRule* rule, int* tags, int numWords) {
  if (rule->v1 >= -1) {
    int i = index + rule->i1;
    if (i >= numWords || i < 0) return false;
    else if (rule->v1 != (rule->t1 ? tags[i] : corpus[i].word)) return false;
  }
  if (rule->v2 >= -1) {
    int i = index + rule->i2;
    if (i >= numWords || i < 0) return false;
    else if (rule->v2 != (rule->t2 ? tags[i] : corpus[i].word)) return false;
  }
  if (rule->v3 >= -1) {
    int i = index + rule->i3;
    if (i >= numWords || i < 0) return false;
    else if (rule->v3 != (rule->t3 ? tags[i] : corpus[i].word)) return false;
  }
  return true;
}

#include <algorithm> // Min

#ifndef min

#define min(a,b) (((a) < (b)) ? (a) : (b))

#endif


#endif