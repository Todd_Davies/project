
#include "kernel.cuh"

Corpus moveCorpusToDevice(vector<WordTag> *corpus, vector<int> *positions, int numWords, int numSentences) {
  Corpus h_corpus;
  h_corpus.numWords = numWords;
  h_corpus.numSentences = numSentences;
  cudaMalloc(&(h_corpus.content), sizeof(WordTag) * numWords);
  cudaMemcpy(h_corpus.content, &(*corpus)[0], sizeof(WordTag) * numWords, cudaMemcpyHostToDevice);
  cudaMalloc(&(h_corpus.positions), sizeof(int) * positions->size());
  cudaMemcpy(h_corpus.positions, &(*positions)[0], sizeof(int) * positions->size(), cudaMemcpyHostToDevice);
  return h_corpus;
}

BrillRule* moveRulesToDevice(unordered_set<BrillRule, BrillRuleHasher> *generatedRules) {
  vector<BrillRule> rules;
  std::copy(generatedRules->begin(), generatedRules->end(), std::back_inserter(rules));
  BrillRule* h_array = &(rules)[0];
  BrillRule* d_array = nullptr;
  cudaMalloc(&d_array, sizeof(BrillRule) * rules.size());
  cudaMemcpy(d_array, h_array, sizeof(BrillRule) * rules.size(), cudaMemcpyHostToDevice);
  return d_array;
}

int* createDeviceIntArray(int numWords) {
  int* d_array = nullptr;
  cudaMalloc(&d_array, sizeof(int) * numWords);
  cudaMemset(d_array, 0, sizeof(int) * numWords);
  return d_array;
}

int* initTagMap(vector<WordTag> *sentence) {
  WordTag* h_array = &(*sentence)[0];
  int* h_tagArray = new int[sentence->size()];
  // Mark the sentence boundaries
  for (size_t i = 0; i < sentence->size(); i++) {
    if (h_array[i].word == -1) h_tagArray[i] = -1;
    else h_tagArray[i] = 0;
  }
  int* d_array = nullptr;
  cudaMalloc(&d_array, sizeof(int) * sentence->size());
  cudaMemcpy(d_array, h_tagArray, sizeof(int) * sentence->size(), cudaMemcpyHostToDevice);
  delete h_tagArray;
  return d_array;
}

/**
 * Load the scores off device memory and into a vector
 */
vector<int>* getScoresOffDevice(int* d_scores, int num) {
  int* h_array = new int[num];
  cudaMemcpy(h_array, d_scores, sizeof(int) * num, cudaMemcpyDeviceToHost);
  vector<int> *out = new vector<int>();
  for (int i = 0; i < num; i++) {
    out->push_back(h_array[i]);
  }
  delete h_array;
  return out;
}

/**
 * Score the given rules 
 * Rules are given +1 if they fix a tag
 * Rules are given -1 if they break a tag
 * If a rule breaks an already broken tag, no net score change is applied.
 */
__global__ void fastScore(const Corpus corpus, const int* tags, BrillRule* rules, const  int numRules, int* net, const int sOff, const int rOff, const int sLength, const int sPerBlock) {
  // Compute what rule this thread is scoring
  int ruleNum = blockIdx.y + rOff;
  if (ruleNum >= numRules) return;
  // Load the rule into shared memory
  __shared__ BrillRule rule;
  rule = rules[ruleNum];
  // Initialise a score counter in shared memory
  __shared__ int score;
  score = 0;
  // Barrier; make sure all threads are here before continuing
  // Avoids resetting the score etc
  __syncthreads();
  // Compute what sentence this thread is scoring
  const int threadSentenceOffset = ((blockIdx.x + sOff) * sPerBlock) + (threadIdx.x / sLength);
  if (threadSentenceOffset > corpus.numSentences) return;
  // Compute what word this thread is scoring
  const int wordPos = corpus.positions[threadSentenceOffset] + (threadIdx.x % sLength);
  if (wordPos >= corpus.numWords) return;
  // Compute the indices in the corpus we're looking at
  const int index1 = wordPos + rule.i1, index2 = wordPos + rule.i2, index3 = wordPos + rule.i3;
  // If the rule applies
  if  ((rule.v1 <= -2 || (index1 < corpus.numWords && index1 >= 0 && rule.v1 == (rule.t1 ? tags[index1] : corpus.content[index1].word)))
    && (rule.v2 <= -2 || (index2 < corpus.numWords && index2 >= 0 && rule.v2 == (rule.t2 ? tags[index2] : corpus.content[index2].word)))
    && (rule.v3 <= -2 || (index3 < corpus.numWords && index3 >= 0 && rule.v3 == (rule.t3 ? tags[index3] : corpus.content[index3].word)))) {
    // Load the current word into registers
    const WordTag currentWord = corpus.content[wordPos];
    // Score the rule
    if (currentWord.tag == tags[wordPos]) {
      if (currentWord.tag != rule.vx) {
        atomicSub(&score, 1);
      }
    }
    else {
      if (currentWord.tag == rule.vx) {
        atomicAdd(&score, 1);
      }
    }
  }
  // Barrier; thread 0 must wait for other threads before writing back to memory
  __syncthreads();
  if (threadIdx.x == 0) atomicAdd(&net[ruleNum], score);
}

/**
 * Parses one line of input from the file
 */
bool lineHandler(char*buf, int l, __int64 pos, vector<WordTag> *sentence, int *numSentences, vector<int> *positions, int *numWords, int* limit){
  if ((*numWords - *numSentences - 1) > *limit) return true;
  if (buf == 0) return false;
  if (l == 0) return false;
  string line = string(buf, l - 1);
  if (line.length() == 2 && line.at(0) == '-' && line.at(1) == '-') {
    sentence->push_back(sent_end);
    (*numSentences)++;
    positions->push_back(*numWords + 1);
  } else {
    int i = line.find(" ");
    string first = line.substr(0, i);
    string second = line.substr(i);
    WordTag wt = { stoi(first), stoi(second) };
    sentence->push_back(wt);
  }
  (*numWords)++;
  return false;
}

/**
 * Calculates the current accuracy of the tagging
 */
double calculateAccuracy(vector<WordTag>* h_corpus, int* d_tags, int numWords, int numSentences) {
  int* tags = new int[numWords];
  cudaMemcpy(tags, d_tags, sizeof(int) * numWords, cudaMemcpyDeviceToHost);
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());
  int total = 0;
  for (int i = 0; i < numWords; i++) {
    if (h_corpus->at(i).tag != -1 && tags[i] == h_corpus->at(i).tag) total++;
  }
  return (100.0 / (numWords - numSentences - 1)) * total;
}

int readCorpus(vector<WordTag> *corpus, vector<int> *sentencePositions, char* bncPath, int *numSentences, int *numWords, int *trainingWords) {
  // Try to open the BNC
  ifstream bnc(bncPath);
  if (bnc.is_open()) {
    // Push a sentence boundary first (for padding)
    corpus->push_back(sent_end);
    sentencePositions->push_back(1);
    // IO Optimised file reading
    readFileFast(bnc, corpus, numSentences, sentencePositions, numWords, trainingWords, lineHandler);
    // Remove the last incomplete sentence
    while (corpus->back().tag != sent_end.tag && corpus->back().word != sent_end.word) {
      corpus->pop_back();
      (*numWords)--;
    }
    // Close the BNC file
    bnc.close();
    // Return the number of *actual* words read (not sentence markers)
    return (*numWords - *numSentences - 1);
  } else {
    return -1;
  }
}

void setupDeviceParameters() {
  // Stop the CPU from doing busy waiting which is the crazy default...
  cudaSetDeviceFlags(cudaDeviceBlockingSync);
}

void usage() {
  cout << "BrillKernels.exe <training_sentences> <sentence_length> <threads_per_block> <corpus_path> <quiet>\n";
}

// http://stackoverflow.com/questions/32530604/how-can-i-get-number-of-cores-in-cuda-device
int getSPcores(cudaDeviceProp devProp) {
  int cores = 0;
  int mp = devProp.multiProcessorCount;
  switch (devProp.major){
  case 2: // Fermi
    if (devProp.minor == 1) cores = mp * 48;
    else cores = mp * 32;
    break;
  case 3: // Kepler
    cores = mp * 192;
    break;
  case 5: // Maxwell
    cores = mp * 128;
    break;
  default:
    printf("Unknown device type\n");
    break;
  }
  return cores;
}

int main(int argc, char* argv[]) {
  int trainingWords, sentenceLength, threadsPerBlock;
  char* bncPath;
  bool quiet;
  if (argc != 6) {
    usage();
    return -1;
  } else {
    sentenceLength = stoi(argv[2]);
    trainingWords = sentenceLength * stoi(argv[1]);
    threadsPerBlock = stoi(argv[3]);
    bncPath = argv[4];
    quiet = strcmp("true", argv[5]) == 0;
  }
  // Print GPU{cores}
  cudaDeviceProp properties;
  cudaGetDeviceProperties(&properties, 0);
  cout << "GPU" << getSPcores(properties) << "\n";
  // Set the CUDA device config
  setupDeviceParameters();
  // Read the corpus
  int numWords = 1, numSentences = 0;
  vector<WordTag> *h_corpus = new vector<WordTag>;
  vector<int> *h_positions = new vector<int>;
  int numWordsRead = readCorpus(h_corpus, h_positions, bncPath, &numSentences, &numWords, &trainingWords);
  if (!quiet) cout << "Read " << numWordsRead << " words.\n";
  // Initialise the tag map in device memory
  int* d_tags = initTagMap(h_corpus);
  // Put the corpus in device memory
  if (!quiet) cout << "Copying corpus to device\n";
  Corpus corpus = moveCorpusToDevice(h_corpus, h_positions, numWords, numSentences);
  // Generate the device rules
  if (!quiet) cout << "Generating rules...";
  unordered_set<BrillRule, BrillRuleHasher>* generatedRules = generateRules(corpus, h_positions, d_tags);
  int numRules = generatedRules->size();
  if (!quiet) cout << numRules << " rules generated.\n";
  // Create the result array
  if (!quiet) cout << "Setting up result arrays\n";
  int *d_scores = createDeviceIntArray(numRules);
  // Put the rules on the device
  if (!quiet) cout << "Moving the rules onto the device...\n";
  // Put the rules on the device
  BrillRule* d_rules = moveRulesToDevice(generatedRules);
  // Score the rules
  if (!quiet) cout << "Scoring rules...\n";
  // Compute number of blocks
  int sentencesPerBlock = threadsPerBlock / sentenceLength;
  int numBlocks = 1 + (numSentences / sentencesPerBlock);
  // Start a timer
  auto start = std::chrono::high_resolution_clock::now();
  for (int ruleOffset = 0; ruleOffset < numRules; ruleOffset += numRules < 65535 ? numRules : 65535) {
    for (int sentenceOffset = 0; sentenceOffset < numBlocks; sentenceOffset += numBlocks < 65535 ? numBlocks : 65535) {
      dim3 gridSize(numBlocks < 65535 ? numBlocks : 65535, numRules < 65535 ? numRules : 65535, 1);
      fastScore<<<gridSize, threadsPerBlock>>>(corpus, d_tags, d_rules, numRules, d_scores, sentenceOffset, ruleOffset, sentenceLength, sentencesPerBlock);
      gpuErrchk(cudaPeekAtLastError());
      gpuErrchk(cudaDeviceSynchronize());
    }
  }
  // Get time to compute & print
  auto finish = std::chrono::high_resolution_clock::now();
  auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
  cout << "TIME: " << ms.count() << "ms\n";
  cout << "NUM_RULES: " << numRules << "\n";
  // Find the index of the best rule
  vector<int> *h_scores = getScoresOffDevice(d_scores, numRules);
  int largestNet = 0, largestIndex = -1;
  for (int i = 0; i < numRules; i++) {
    if (h_scores->at(i) > largestNet) {
      largestNet = h_scores->at(i);
      largestIndex = i;
    }
  }
  if (largestIndex == -1) {
    if (!quiet) cout << "All rules had negative scores!";
  } else {
    if (!quiet) cout << "best score: " << h_scores->at(largestIndex) << "\n";
  }
  // Get the best rule off the device
  BrillRule bestRule;
  cudaMemcpy(&bestRule, &(d_rules[largestIndex]), sizeof(BrillRule), cudaMemcpyDeviceToHost);
  if (!quiet) printRule(&bestRule);

  // Apply rule
  applyRule(corpus, d_tags, bestRule, h_positions);

  // Calculate accuracy
  if (!quiet) cout << calculateAccuracy(h_corpus, d_tags, numWords, numSentences) << "%%\n";
  // Free the memory
  delete generatedRules;
  delete h_positions;
  cudaFree(d_scores);
  free(h_scores);
  free(h_corpus);
}