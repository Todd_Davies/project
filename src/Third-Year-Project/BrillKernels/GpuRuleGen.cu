#include "GpuRuleGen.cuh"

// Set the cuda dimensions for processing rule generation and application
const dim3 gridSize(32, 1, 1);
const dim3 blockSize(192, 1, 1);
const int num_threads = (gridSize.x * gridSize.y * gridSize.z) * (blockSize.x * blockSize.y * blockSize.z);
// Buffer length (dependent on GPU memory size)
int buffer_len = -1;

/**
 * Move an integer array onto the device
 */
int* moveToDevice(int* h_arr, int len) {
  int *d_arr = nullptr;
  cudaMalloc(&d_arr, sizeof(int) * len);
  cudaMemcpy(d_arr, h_arr, sizeof(int) * len, cudaMemcpyHostToDevice);
  delete h_arr;
  return d_arr;
}

/**
* Determine how much free memory we have on the GPU so that we can maximise the buffer length
*/
int determineBufferLength(int num_threads) {
  size_t free_byte, total_byte;
  cudaError_t cuda_status = cudaMemGetInfo(&free_byte, &total_byte);
  if (cudaSuccess != cuda_status){
    printf("Error: cudaMemGetInfo fails, %s \n", cudaGetErrorString(cuda_status));
    exit(1);
  }
  return 0.9 * free_byte / (num_threads * sizeof(BrillRule));
}

// Store the rule templates in constant memory for fast (cached) access
__constant__ BrillRule c_templates[num_templates];

/**
 * Generates rules based on errors in the existing corpus.
 */
__global__ void generateRules(BrillRule* buffer, int buffer_len, int* currentPos, int* endPos, int num_templates, Corpus corpus, int* tags) {
  const int blockNum = blockIdx.x + (blockIdx.y * blockDim.x);
  const int threadId = (blockNum * blockDim.x) + threadIdx.x;
  const int end = endPos[threadId];
  int i, bufferIdx;
  for (i = currentPos[threadId], bufferIdx = 0; i < end; i++) {
    if (corpus.content[i].tag != -1 && corpus.content[i].tag != tags[i]) {
      for (int ruleNum = 0; ruleNum < num_templates; ruleNum++, bufferIdx++) {
        BrillRule tmpl = c_templates[ruleNum];
        buffer[(threadId * buffer_len) + bufferIdx] = {
          tmpl.t1, tmpl.t2, tmpl.t3,
          tmpl.i1, tmpl.i2, tmpl.i3,
          tmpl.v1 == -2 || (i + tmpl.i1) < 0 || (i + tmpl.i1) >= corpus.numWords ? -2 : (tmpl.t1 ? tags[i + tmpl.i1] : corpus.content[i + tmpl.i1].word),
          tmpl.v2 == -2 || (i + tmpl.i2) < 0 || (i + tmpl.i2) >= corpus.numWords ? -2 : (tmpl.t2 ? tags[i + tmpl.i2] : corpus.content[i + tmpl.i2].word),
          tmpl.v3 == -2 || (i + tmpl.i3) < 0 || (i + tmpl.i3) >= corpus.numWords ? -2 : (tmpl.t3 ? tags[i + tmpl.i3] : corpus.content[i + tmpl.i3].word),
          corpus.content[i].tag };
      }
      // Check we've got room
      if ((buffer_len - bufferIdx) < num_templates) {
        i++;
        break;
      }
    }
  }
  currentPos[threadId] = i;
}

/**
 * Applies a rule to the corpus on the device
 */
__global__ void applyRule(int* currentPos, int* endPos, Corpus corpus, int* tags, BrillRule toApply) {
  const int blockNum = blockIdx.x + (blockIdx.y * blockDim.x);
  const int threadId = (blockNum * blockDim.x) + threadIdx.x;
  const int end = endPos[threadId];
  for (int i = currentPos[threadId]; i < end; i++) {
    if (doesApply(corpus.content, i, &toApply, tags, corpus.numWords)) {
      tags[i] = toApply.vx;
    }
  }
}

/**
 * Pulls the generated rules off the device and adds them to the set.
 * Returns true if all the threads finished.
 */
bool addRules(std::unordered_set<BrillRule, BrillRuleHasher> *rules, BrillRule* d_rules) {
  if (buffer_len < 0) return true;
  BrillRule* h_rules = new BrillRule[num_threads * buffer_len];
  cudaMemcpy(h_rules, d_rules, sizeof(BrillRule) * num_threads * buffer_len, cudaMemcpyDeviceToHost);
  bool finished = true;
  for (int i = 0; i < num_threads; i++) {
    for (int j = 0; j < buffer_len; j++) {
      BrillRule rule = h_rules[(i * buffer_len) + j];
      if (rule.vx != -1) {
        if (rules->find(rule) == rules->end()) rules->insert(rule);
        if (j + 1 == buffer_len) finished = false;
      }
      else break;
    }
  }
  delete h_rules;
  return finished;
}

/**
 * Compute how many sentences each thread should take
 */
void computeThreadWork(int **d_currentPos, int **d_endPos, std::vector<int> *h_positions, int numSentences) {
  int baseSentences = numSentences / num_threads, extra = numSentences % num_threads;
  int *h_currentPos = new int[num_threads], *h_endPos = new int[num_threads];
  int p = 0;
  for (int i = 0; i < num_threads; i++) {
    h_currentPos[i] = h_positions->at(p);
    h_endPos[i] = h_positions->at(p += (baseSentences + ((i < extra) ? 1 : 0)));
  }
  *d_currentPos = moveToDevice(h_currentPos, num_threads);
  *d_endPos = moveToDevice(h_endPos, num_threads);
}

std::unordered_set<BrillRule, BrillRuleHasher>* generateRules(Corpus corpus, std::vector<int> *h_positions, int* tags) {
  std::unordered_set<BrillRule, BrillRuleHasher> *generated_rules = new std::unordered_set<BrillRule, BrillRuleHasher>;
  // Create the device buffers
  buffer_len = determineBufferLength(num_threads);
  BrillRule* d_buffer = nullptr;
  cudaMalloc(&d_buffer, sizeof(BrillRule) * buffer_len * num_threads);
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  // Compute how many sentences each thread should take
  int *d_currentPos = nullptr, *d_endPos = nullptr;
  computeThreadWork(&d_currentPos, &d_endPos, h_positions, corpus.numSentences);

  // Transfer the templates onto the device
  BrillRule* d_templates = nullptr;
  cudaMemcpyToSymbol(c_templates, templates, sizeof(BrillRule) * num_templates);

  // While we've not processed the whole corpus
  do {
    // Zero the buffer
    cudaMemset(d_buffer, -1, sizeof(BrillRule) * buffer_len * num_threads);
    // Generate the rules
    generateRules << <gridSize, blockSize>> >(d_buffer, buffer_len, d_currentPos, d_endPos, num_templates, corpus, tags);
    gpuErrchk(cudaPeekAtLastError());
    gpuErrchk(cudaDeviceSynchronize());
  // Add the rules to the set
  } while (!addRules(generated_rules, d_buffer));

  // Free the device memory
  cudaFree(d_buffer);
  cudaFree(d_templates);
  cudaFree(d_currentPos);
  cudaFree(d_endPos);
  // Put rules onto device
  return generated_rules;
}

void applyRule(Corpus corpus, int *d_tags, BrillRule rule, std::vector<int> *h_positions) {
  // Compute how many sentences each thread should take
  int *d_currentPos = nullptr, *d_endPos = nullptr;
  computeThreadWork(&d_currentPos, &d_endPos, h_positions, corpus.numSentences);

  // Apply the rule
  applyRule << <gridSize, blockSize >> >(d_currentPos, d_endPos, corpus, d_tags, rule);
  gpuErrchk(cudaPeekAtLastError());
  gpuErrchk(cudaDeviceSynchronize());

  // Free the other data
  cudaFree(d_currentPos);
  cudaFree(d_endPos);
}