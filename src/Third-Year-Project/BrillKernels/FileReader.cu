#include "FileReader.cuh"

void readFileFast(ifstream &file, vector<WordTag> *corpus, int *numSentences,
  vector<int> *positions, int *numWords, int* limit,
  bool(*lineHandler)(char*str, int length, __int64 absPos, vector<WordTag> *sentence,
  int *numSentences, vector<int> *positions, int *numWords, int* limit)) {
  int BUF_SIZE = 40000;
  file.seekg(0, ios::end);
  ifstream::pos_type p = file.tellg();
#ifdef WIN32
  __int64 fileSize = *(__int64*)(((char*)&p) + 8);
#else
  __int64 fileSize = p;
#endif
  file.seekg(0, ios::beg);
  BUF_SIZE = min(BUF_SIZE, fileSize);
  char* buf = new char[BUF_SIZE];
  int bufLength = BUF_SIZE;
  file.read(buf, bufLength);

  int strEnd = -1;
  int strStart;
  __int64 bufPosInFile = 0;
  while (bufLength > 0) {
    int i = strEnd + 1;
    strStart = strEnd;
    strEnd = -1;
    for (; i < bufLength && i + bufPosInFile < fileSize; i++) {
      if (buf[i] == '\n') {
        strEnd = i;
        break;
      }
    }

    if (strEnd == -1) { // scroll buffer
      if (strStart == -1) {
        if (lineHandler(buf + strStart + 1, bufLength, bufPosInFile + strStart + 1, corpus, numSentences, positions, numWords, limit)) {
          return;
        }
        bufPosInFile += bufLength;
        bufLength = min(bufLength, fileSize - bufPosInFile);
        delete[]buf;
        buf = new char[bufLength];
        file.read(buf, bufLength);
      }
      else {
        int movedLength = bufLength - strStart - 1;
        memmove(buf, buf + strStart + 1, movedLength);
        bufPosInFile += strStart + 1;
        int readSize = min(bufLength - movedLength, fileSize - bufPosInFile - movedLength);

        if (readSize != 0)
          file.read(buf + movedLength, readSize);
        if (movedLength + readSize < bufLength) {
          char *tmpbuf = new char[movedLength + readSize];
          memmove(tmpbuf, buf, movedLength + readSize);
          delete[]buf;
          buf = tmpbuf;
          bufLength = movedLength + readSize;
        }
        strEnd = -1;
      }
    }
    else {
      if (lineHandler(buf + strStart + 1, strEnd - strStart, bufPosInFile + strStart + 1, corpus, numSentences, positions, numWords, limit)) {
        return;
      }
    }
  }
  lineHandler(0, 0, 0, corpus, numSentences, positions, numWords, limit);//eof
}