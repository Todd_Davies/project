#include <iostream>
#include <vector>

#include "Defns.cuh"

using namespace std;

void readFileFast(ifstream &file, vector<WordTag> *corpus, int *numSentences,
  vector<int> *positions, int *numWords, int* limit,
  bool(*lineHandler)(char*str, int length, __int64 absPos, vector<WordTag> *sentence,
  int *numSentences, vector<int> *positions, int *numWords, int* limit));