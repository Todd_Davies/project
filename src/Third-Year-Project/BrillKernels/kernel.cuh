#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "device_functions.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <deque>
#include <algorithm>    // std::min
#include <unordered_map>
#include <chrono>

// Sleep 
#include <windows.h>

#include "Defns.cuh"
#include "GpuRuleGen.cuh"
#include "FileReader.cuh"

using namespace std;