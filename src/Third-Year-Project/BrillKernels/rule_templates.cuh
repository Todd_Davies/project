#ifndef BRILL_TEMPLATES
#define BRILL_TEMPLATES

#include "Defns.cuh"

// -2 value = sub-rule doesn't apply
/*const static BrillRule templates[] = {
                          { true, false, false, 0, 0, 0, 0, -2, -2, 0 },
                          { true, false, false, 1, 0, 0, 0,-2,-2, 0 },
                          { true, false, false,-2, 0, 0, 0,-2,-2, 0 },
                          { true, false, false, 2, 0, 0, 0,-2,-2, 0 },
                          { true, true, false,  0, 1, 0, 0, 0,-2, 0 },
                          { true, true, false,  0,-1, 0, 0, 0,-2, 0 },
                          { true, true, true,   0,-1,-2, 0, 0, 0, 0 },
                          { true, true, true,   0, 1, 2, 0, 0, 0, 0 },
                          { true, true, true,   0, 1,-1, 0, 0, 0, 0 },
                          { true, true, false,  1,-2, 0, 0, 0,-2, 0 },

                          { true, true, false, 1,  2, 0, 0, 0,-2, 0 },
                          { true, true, true,  -3,-2,-1, 0, 0, 0, 0 },
                          { true, true, true,  -3,-2,-1, 0, 0, 0, 0 },
                          { true, true, true,   3, 2, 1, 0, 0, 0, 0 },
                          { true, true, false, -1, 1, 0, 0, 0,-2, 0 },
                          { true, true, false, -2,-1, 0, 0, 0,-2, 0 },
                          { true, true, false,  2, 1, 0, 0, 0,-2, 0 },

                          { false, true, false, -1, 0, 0, 0, -2, -2, 0 },
                          { false, true, false,  1, 0, 0, 0, -2, -2, 0 }, 
                          { false, true, false,  2, 0, 0, 0, -2, -2, 0 },
                          { false, true, false, -2, 0, 0, 0, -2, -2, 0 },
                          { false, true, false, -1, -2, 0, 0, 0, -2, 0 },
                          { false, true, false,  1, 2, 0, 0,  0, -2, 0 },
                          { false, true, false, -1, 0, 0, 0,  0, -2, 0 },
                          { false, true, false,  1, 0, 0, 0, -2, -2, 0 },
                          { false, true, false,  0, 0, 0, 0, -2, -2, 0 },
                          { false, true, false, -1, -1, 0, 0, 0, -2, 0 },
                          { false, true, false, -1, -1, 0, 0, 0, -2, 0 },
                          { false, true, false, -1,  1, 0, 0, 0, -2, 0 },
                          { false, false, true,  0, -1,-1, 0, 0, 0, 0 },
                          { false, false, true, 0,  1,  1, 0, 0, 0, 0 }, };
const static int num_templates = 32;*/

/*const static BrillRule templates[] = {
  { true, false, false,  0, 0, 0, 0, -2, -2, 0 },
  { true, false, false, -1, 0, 0, 0, -2, -2, 0 },
};
const static int num_templates = 2;*/

const static int num_templates = 10;
const static BrillRule templates[] = {
  { true, false, false, 0, 0, 0, 0, -2, -2, 0 },
  { true, true, false, 1, 0, 0, 0, 0, -2, 0 },
  { true, true, false, -1, 0, 0, 0, 0, -2, 0 },
  { true, true, true, -2, -1, 0, 0, 0, 0, 0 },
  { true, true, true, 2, 1, 0, 0, 0, 0, 0 },
  { true, true, true, 1, -1, 0, 0, 0, -2, 0 },
  { false, true, true, 0, 0, 0, 0, -2, -2, 0 },
  { false, true, true, 0, -1, 0, 0, 0, -2, 0 },
  { false, true, true, 0, 1, 0, 0, 0, -2, 0 },
  { false, true, true, 0, 1, -1, 0, 0, 0, 0 } };
#endif