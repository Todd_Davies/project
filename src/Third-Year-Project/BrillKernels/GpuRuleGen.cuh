#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "device_functions.h"

#include "Defns.cuh"
#include "rule_templates.cuh"

#include <unordered_set>
#include <vector>

// Generate rules based on the errors in the corpus
std::unordered_set<BrillRule, BrillRuleHasher>* generateRules(Corpus d_corpus, std::vector<int> *h_positions, int* tags);

// Apply the rule, don't generate more rules
void applyRule(Corpus corpus, int *d_tags, BrillRule rule, std::vector<int> *h_positions);