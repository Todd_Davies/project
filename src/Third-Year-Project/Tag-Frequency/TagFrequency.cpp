#include "TagFrequency.h"

namespace tag_frequency {

  string getMaxTag(unordered_map<string, int> count) {
    int largest = -1;
    // Set the initial tag
    string out = "0";//"UNK";
    for (auto kv : count) {
      if (kv.second > largest) {
        out = kv.first;
        largest = kv.second;
      }
    }
    return out;
  }

  string TagFrequency::getMostCommonTag(string word) {
    pair<string, int> largest = { "", -1 };
    pair<multimap<string, string>::iterator, multimap<string, string>::iterator> ret;
    ret = tagMap.equal_range(word);
    for (multimap<string, string>::iterator it = ret.first; it != ret.second; ++it) {
      int count = countMap[{word, it->second}];
      if (count > largest.second) {
        largest = { it->second, count };
      }
    }
    // Can be "UNK" or getMaxTag(tagCount) depending on mode
    if (largest.first.compare("") == 0) return "0";
    else return largest.first;
  }

  void TagFrequency::add(TrainingWord word) {
    pair<string, string> p = { word.getWord(), word.getTag() };
    countMap[p]++;
    if (tagMap.find(p.first) == tagMap.end()) tagMap.insert(p);
    tagCount[word.getTag()]++;
  }
}