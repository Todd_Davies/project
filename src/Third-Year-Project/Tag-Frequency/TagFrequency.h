#ifndef TAG_FREQUENCY
#define TAG_FREQUENCY

#include "BncParser.h"

#include <map>
#include <unordered_map>

using namespace std;
using namespace bnc_parser;

namespace tag_frequency {

  class TagFrequency {
  private:
    // Produces a hash code for a pair of strings
    struct PairHasher {
      size_t operator()(const pair<string, string>& t) const {
        return hash<string>()(t.first) + hash<string>()(t.second);
      }
    };

    unordered_map<string, int> tagCount;
    multimap<string, string> tagMap;
    unordered_map<pair<string, string>, int, PairHasher> countMap;

  public:
    // Gets the most common tag for a word based on the current training data
    string getMostCommonTag(string word);
    // Adds a word to the training data
    void add(TrainingWord word);
  };

}
#endif