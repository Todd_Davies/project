#ifndef BNC_PARSE
#define BNC_PARSE

#include "stdafx.h"

#include <stdlib.h>
#include <vector>
#include <concurrent_unordered_set.h>
#include <iterator>
#include <iostream>
#include <fstream>

#include "boost/filesystem.hpp"
#include "boost/tuple/tuple.hpp"

#include <unordered_set>

using namespace concurrency;
using namespace std;
using namespace boost::filesystem;

namespace bnc_parser {

  struct CharHasher {
    size_t operator()(const char* &t) const {
      std::hash<class T*> h;
      return h((T*)t);
    }
    size_t operator()(char* t) const {
      return hash<string>()(t);
    }
  };

  struct CharEquals
  {
    bool operator()(char* s1, char* s2) const
    {
      return strcmp(s1, s2) == 0;
    }
  };

  typedef concurrent_unordered_set<char*, CharHasher, CharEquals> StringPool;

  class TrainingWord {
  private:
    const char* word;
    const char* trainingTag;
    const char* actualTag;
  public:
    TrainingWord(const char* word, const char* trainingTag, const char* actualTag);
    string getWord();
    const char* getWordChars();
    string getTrainingTag();
    const char* getTrainingTagChars();
    string getTag();
    const char* getTagChars();
    void TrainingWord::setTrainingTag(string t, concurrent_unordered_set<char*, CharHasher, CharEquals> *tags);
    void TrainingWord::setTrainingTag(char* t);
    void TrainingWord::setTag(string t);
    void TrainingWord::setTag(char* t);
  };

  vector<TrainingWord>* readSentence(vector<TrainingWord>* out, ifstream* file);
  
  //
  // Iterates over a BNC corpus.
  // Not thread safe!
  // Sample usage:
  // for (BncIterator iterator = BncIterator("c:/path/to/bnc");
  //      !iterator.isEmpty(); iterator++) {
  //   vector<TaggedWord>* sentence = *iterator;
  // }
  //
  class BncIterator : public std::iterator <std::input_iterator_tag, vector<TrainingWord>*> {
  private:
    //
    // Iterates over the files inside the BNC corpus directory.
    // Goes in reverse order (from K to A).
    //
    class FileIterator : public std::iterator <std::input_iterator_tag, std::string> {
    private:
      stack<path> queue;
      directory_iterator endItr;
      directory_iterator itr;
      // Moves the iterator to the next file, queueing any directories it sees along the way.
      void moveToNextFile();

    public:
      FileIterator(string filename, bool altFormat);
      FileIterator operator++(int);
      string operator*();
      bool isEmpty();
    };

    bool altFormat;
    ifstream* altFile;
    BncIterator plusplus(int);

    bool openNextFile();
    void readNextSentence();
    ifstream *openFile = nullptr;
    vector<TrainingWord> currentSentence;
    FileIterator files;
    concurrent_unordered_set<char*, CharHasher, CharEquals> *tags = nullptr, *words = nullptr;

  public:
    BncIterator(string filename, concurrent_unordered_set<char*, CharHasher, CharEquals> *tags, concurrent_unordered_set<char*, CharHasher, CharEquals> *words, bool altFormat);
    BncIterator operator++(int);
    vector<TrainingWord>* operator*();
    bool isEmpty();
    void close();
  };

}

#endif