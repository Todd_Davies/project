#include "stdafx.h"

#include <iostream>

#include "boost/filesystem.hpp"   // includes all needed Boost.Filesystem declarations

#include "BncParser.h"

namespace bnc_parser {

  // ================
  // Training word
  // ================

  TrainingWord::TrainingWord(const char* word, const char* trainingTag, const char* actualTag) :
    word(word), trainingTag(trainingTag), actualTag(actualTag) {};
  string TrainingWord::getWord() { return word; }
  const char* TrainingWord::getWordChars() { return word; }
  string TrainingWord::getTrainingTag() { return trainingTag; }
  const char* TrainingWord::getTrainingTagChars() { return trainingTag; }
  string TrainingWord::getTag() { return actualTag; }
  const char* TrainingWord::getTagChars() { return actualTag; }
  void TrainingWord::setTrainingTag(string t, StringPool *tags) { 
    char* charArray = new char[t.length() + 1];
    strcpy(charArray, t.c_str());
    auto tag = tags->find(charArray);
    if (tag != tags->end()) {
      delete charArray;
      charArray = *tag;
    }
    else {
      tags->insert(charArray);
    }
    trainingTag = charArray;
  }
  void TrainingWord::setTrainingTag(char* t) { trainingTag = t; }
  void TrainingWord::setTag(string t) { actualTag = t.c_str(); }
  void TrainingWord::setTag(char* t) { actualTag = t; }

  // ================
  // Useful functions
  // ================

  int findChar(const string *s, const char c) {
    for (unsigned int i = 0; i < s->length(); i++) {
      if (s->at(i) == c) return i;
    }
    return -1;
  }

  bool isSentenceMarker(string line) {
    return line.length() < 2 || ((line[0] == '<' && line[1] == 'p'));
  }

  // TODO: If more efficiency is needed, use memory mapped IO:
  // http://stackoverflow.com/questions/17925051/fast-textfile-reading-in-c

  vector<TrainingWord>* readSentence(vector<TrainingWord>* out, ifstream* file, StringPool *tags, StringPool *words) {
    string line = "";
    getline(*file, line);
    TrainingWord *word = nullptr;
    while (!file->eof()) {
      if (isSentenceMarker(line)) {
        if (out->size() > 0) break;
        else getline(*file, line);;
      }
      string word, tag;
      if (line.at(3) != '!') {
        word = line.substr(9);
        tag = line.substr(0, 7);
      } else {
        word = line.substr(5);
        tag = line.substr(0, 3);
      }
      char* cWord = new char[word.length() + 1];
      strcpy(cWord, word.c_str());
      auto possibleWord = words->find(cWord);
      if (possibleWord != words->end()) {
        delete cWord;
        cWord = *possibleWord;
      }
      else {
        words->insert(cWord);
      }
      char* cTag = new char[tag.length() + 1];
      strcpy(cTag, tag.c_str());
      auto possibleTag = tags->find(cTag);
      if (possibleTag != tags->end()) {
        delete cTag;
        cTag = *possibleTag;
      } else {
        tags->insert(cTag);
      }
      out->push_back(TrainingWord(cWord, "", cTag));
      getline(*file, line);
    }
    return out;
  }

  vector<TrainingWord>* readAltSentence(vector<TrainingWord>* out, ifstream* file, StringPool *tags, StringPool *words) {
    string line = "";
    getline(*file, line);
    TrainingWord *word = nullptr;
    while (!file->eof()) {
      if (line.at(0) == '-' && line.at(1) == '-') {
        if (out->size() > 0) break;
        else getline(*file, line);
      }
      string word, tag;
      if (line.at(3) == '-') {
        word = line.substr(8);
        tag = line.substr(0, 7);
      }
      else {
        word = line.substr(4);
        tag = line.substr(0, 3);
      }
      char* cWord = new char[word.length() + 1];
      strcpy(cWord, word.c_str());
      auto possibleWord = words->find(cWord);
      if (possibleWord != words->end()) {
        delete cWord;
        cWord = *possibleWord;
      }
      else {
        words->insert(cWord);
      }
      char* cTag = new char[tag.length() + 1];
      strcpy(cTag, tag.c_str());
      auto possibleTag = tags->find(cTag);
      if (possibleTag != tags->end()) {
        delete cTag;
        cTag = *possibleTag;
      }
      else {
        tags->insert(cTag);
      }
      out->push_back(TrainingWord(cWord, "", cTag));
      getline(*file, line);
    }
    return out;
  }

  vector<TrainingWord>* readAltSentenceInts(vector<TrainingWord>* out, ifstream* file, StringPool *tags, StringPool *words) {
    string line = "";
    getline(*file, line);
    TrainingWord *word = nullptr;
    while (!file->eof()) {
      if (line.at(0) == '-' && line.at(1) == '-') {
        if (out->size() > 0) break;
        else getline(*file, line);
      }
      int split = 1;
      for (size_t i = 0; i < line.length(); i++) {
        if (line.at(i) == ' ') {
          split = i; break;
        }
      }
      string tag = line.substr(split + 1);
      string word = line.substr(0, split);
      char* cWord = new char[word.length() + 1];
      strcpy(cWord, word.c_str());
      auto possibleWord = words->find(cWord);
      if (possibleWord != words->end()) {
        delete cWord;
        cWord = *possibleWord;
      }
      else {
        words->insert(cWord);
      }
      char* cTag = new char[tag.length() + 1];
      strcpy(cTag, tag.c_str());
      auto possibleTag = tags->find(cTag);
      if (possibleTag != tags->end()) {
        delete cTag;
        cTag = *possibleTag;
      }
      else {
        tags->insert(cTag);
      }
      out->push_back(TrainingWord(cWord, "", cTag));
      getline(*file, line);
    }
    return out;
  }

  // ================
  // File iterator
  // ================

  void BncIterator::FileIterator::moveToNextFile() {
    while (!isEmpty()) {
      if (itr == endItr) {
        itr = directory_iterator(queue.top());
        queue.pop();
      }
      if (is_directory(itr->path())) {
        queue.push(itr->path());
        itr++;
      }
      else return;
    }
  }

  BncIterator::FileIterator::FileIterator(string filename, bool alt) {
    if (!alt) {
      queue = stack<path>();
      queue.push(path(filename));
      moveToNextFile();
    }
  }

  BncIterator::FileIterator BncIterator::FileIterator::operator++(int) {
    itr++;
    moveToNextFile();
    return *this;
  }

  string BncIterator::FileIterator::operator*() {
    if (itr != endItr) return itr->path().string();
    else return "";
  }


  bool BncIterator::FileIterator::isEmpty() {
    return queue.size() == 0 && itr == endItr;
  }
  
  // =================
  // Sentence iterator
  // =================
  BncIterator::BncIterator(string filename, StringPool *tags, StringPool *words, bool altFormat) : files(filename, altFormat), currentSentence(), tags(tags), words(words), altFormat(altFormat) {
    if (altFormat) {
      altFile = new ifstream();
      altFile->open(filename);
    }
    (*this)++;
  }
  
  bool BncIterator::openNextFile() {
    if (files.isEmpty()) {
      openFile = nullptr;
      return false;
    } else {
      openFile = new ifstream();
      openFile->open(*files);
      files++;
      return true;
    }
  }

  void BncIterator::readNextSentence() {
    // Open file == nullptr means that it's the first iteration
    // So we try and open the next file. If it fails, we return.
    if (openFile == nullptr && !openNextFile()) return;
    else {
      currentSentence.clear();
      readSentence(&currentSentence, openFile, tags, words);
      if (openFile->eof()) {
        openFile->close();
        delete openFile;
        if (!openNextFile()) return;
      }
    }
  }

  BncIterator BncIterator::plusplus(int) {
    currentSentence.clear();
    if (altFile != nullptr) {
      // Read next sentence
      readAltSentenceInts(&currentSentence, altFile, tags, words);
      // Close if we're finished
      if (altFile->eof()) {
        altFile->close();
        delete altFile;
      }
    }
    return *this;
  }

  BncIterator BncIterator::operator++(int) {
    if (altFormat) return this->plusplus(0);
    if (files.isEmpty() && openFile == nullptr) currentSentence.clear();
    else readNextSentence();
    return *this;
  }

  vector<TrainingWord>* BncIterator::operator*() {
    return &currentSentence;
  }

  bool BncIterator::isEmpty() {
    return altFormat ? altFile == nullptr : files.isEmpty() && currentSentence.size() == 0;
  }

  void BncIterator::close() {
    delete openFile;
  }

}