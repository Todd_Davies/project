#include <boost/test/unit_test.hpp>

#include "stdafx.h"
#include "TestData.h"
#include "BrillTaggerRules.h"
#include "RuleSchema.h"
#include "BrillTaggerTrainer.h"

using namespace brill_tagger;

BOOST_AUTO_TEST_CASE(BrillRuleGenTest)
{
  cout << "Starting rule generation test\n";
  // Read some rules
  string path = "C:\\temp\\BrillRulesTest.tmpl";
  ofstream outputFile(path);
  for (int i = 0; i < NUM_RULE_TEMPLATES; i++) {
    outputFile << RULE_TEMPLATES[i] << "\n";
  }
  outputFile.close();
  Parser p;
  vector<Rule*> rules = p.parseTemplateFile(path);
  // Make a base tagger
  TagFrequency base;
  concurrent_unordered_set<char*, CharHasher, CharEquals> tags, words;
  BncIterator bnc = BncIterator("C:\\Users\\Todd\\dev\\project\\data\\BNC\\BNC\\B", &tags, &words, false);
  int i = 0;
  cout << "Reading 500 sentences for base tagger\n";
  while (!bnc.isEmpty() && i++ < 500) {
    vector<TrainingWord> *sentence = *bnc;
    for (TrainingWord t : *sentence) base.add(t);
    bnc++;
  }
  cout << "Training the rules\n";
  // Train the rules
  int numRules;
  vector<Rule*> instansiatedRules = trainRules(rules, &bnc, &base, &tags, &words, 10, 1, 0, 1, &numRules);
  cout << "Done\n";
  bnc.close();
  for (Rule* r : rules) delete r;
  for (Rule *r : instansiatedRules) delete r;
  for (char* tag : tags) free(tag);
  for (char* word: words) free(word);
}