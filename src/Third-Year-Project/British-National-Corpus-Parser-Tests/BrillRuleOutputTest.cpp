#include <boost/test/unit_test.hpp>
#include <boost/bind.hpp>

#include "stdafx.h"
#include "TestData.h"
#include "BrillTaggerRules.h"
#include "RuleSchema.h"
#include "BrillTaggerTrainer.h"

using namespace brill_tagger;

void writeBncToFile(string filename, TrainingWord *data, int len) {
  ofstream file(filename);
  file << "<p><a name=\"000\">\n";
  for (int i = 0; i < len; i++) {
    file << data[i].getTag() << "!!" << data[i].getWord() << "\n";
  }
  file.close();
}

BOOST_AUTO_TEST_CASE(BrillRuleOutputTest)
{
  cout << "Starting rule gen serial output test\n";
  // Read some rules
  string path = "C:\\temp\\BrillRulesTest.tmpl";
  ofstream outputFile(path);
  for (int i = 0; i < NUM_RULE_TEMPLATES; i++) {
    outputFile << RULE_TEMPLATES[i] << "\n";
  }
  outputFile.close();
  Parser p;
  vector<Rule*> rules = p.parseTemplateFile(path);
  // Make a base tagger but don't train it
  TagFrequency base;
  concurrent_unordered_set<char*, CharHasher, CharEquals> tags, words;
  // Write a sample bnc
  string root = "C:\\temp\\BncTest\\";
  // Create blank files
  for (int i = 0; i < TEST_FILES_NUM; i++) {
    ofstream file;
    writeBncToFile(root + TEST_FILES[i].name, TEST_FILES[i].content, TEST_FILES[i].len);
    file.close();
  }
  bnc_parser::BncIterator bnc(root, &tags, &words, false);
  // Train the rules
  int numRules = 0;
  cout << "Generating rules...";
  vector<Rule*> instansiatedRules = trainRules(rules, &bnc, &base, &tags, &words, 10, 1, 0, 1, &numRules);
  cout << "Done\n";
  // 85 rules should be tested
  BOOST_REQUIRE_EQUAL(numRules, 85);
  bnc.close();
  for (Rule* r : rules) delete r;
  for (Rule *r : instansiatedRules) delete r;
  for (char* tag : tags) free(tag);
  for (char* word : words) free(word);
}

BOOST_AUTO_TEST_CASE(BrillParallelRuleOutputTest)
{
  cout << "Starting rule gen parallel output test\n";
  // Read some rules
  string path = "C:\\temp\\BrillRulesTest.tmpl";
  ofstream outputFile(path);
  for (int i = 0; i < NUM_RULE_TEMPLATES; i++) {
    outputFile << RULE_TEMPLATES[i] << "\n";
  }
  outputFile.close();
  Parser p;
  vector<Rule*> rules = p.parseTemplateFile(path);
  // Make a base tagger but don't train it
  TagFrequency base;
  concurrent_unordered_set<char*, CharHasher, CharEquals> tags, words;
  // Write a sample bnc
  string root = "C:\\temp\\BncTest\\";
  // Create blank files
  for (int i = 0; i < TEST_FILES_NUM; i++) {
    ofstream file;
    writeBncToFile(root + TEST_FILES[i].name, TEST_FILES[i].content, TEST_FILES[i].len);
    file.close();
  }
  bnc_parser::BncIterator bnc(root, &tags, &words, false);
  // Train the rules
  int numRules = 0;
  cout << "Generating rules...";
  vector<Rule*> instansiatedRules = trainRulesParallel(rules, &bnc, &base, &tags, &words, 10, 1, 0, 1, &numRules);
  cout << "Done\n";
  // 85 rules should be tested
  BOOST_REQUIRE_EQUAL(numRules, 85);
  bnc.close();
  for (Rule* r : rules) delete r;
  for (Rule *r : instansiatedRules) delete r;
  for (char* tag : tags) free(tag);
  for (char* word : words) free(word);
}