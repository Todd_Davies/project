#include <boost/test/unit_test.hpp>

#include "stdafx.h"
#include "TestData.h"
#include "BrillTaggerRules.h"

using namespace brill_tagger;

BOOST_AUTO_TEST_CASE(TestRules)
{
  cout << "Starting rules tests...\n";
  Rule templateRule;
  RulePrerequisite *pre = new RulePrerequisite();
  pre->setWordType(false);
  pre->setRelativeIndex(0);
  templateRule.addPrerequisite(pre);
  pre = new RulePrerequisite();
  pre->setWordType(false);
  pre->setRelativeIndex(-1);
  pre->setMatchLength(2);
  templateRule.addPrerequisite(pre);
  vector<TrainingWord> sentence;
  for (int i = 0; i < TEST_DATA_LENGTH; i++) sentence.push_back(TEST_DATA[i]);
  // Make sets for the words etc
  concurrent_unordered_set<char*, CharHasher, CharEquals> tags, words;
  // Change the tag for testing
  char* bogusTag = new char[4]{'B', 'O', 'B', '\0'};
  sentence[2].setTag(bogusTag);
  tags.insert(bogusTag);
  // Instansiate returns a new Rule
  Rule *myRule = templateRule.instantiate(&sentence, 2, &tags, &words);
  cout << "Testing rule generation\n";
  BOOST_REQUIRE_EQUAL(myRule->toString(), "(TAG[0]='NP0'&TAG[-1][0:2]='NP1')->'BOB'");
  cout << "Testing rule application\n";
  for (int i = 0; i < TEST_DATA_LENGTH; i++) {
    myRule->apply(&sentence, i);
    BOOST_REQUIRE_EQUAL(sentence[i].getTrainingTag(), i == 2 ? bogusTag : TEST_DATA[i].getTrainingTag());
    BOOST_REQUIRE_EQUAL(sentence[i].getWord(), TEST_DATA[i].getWord());
  }
  cout << "Finished rule tests\n";
  delete myRule;
}