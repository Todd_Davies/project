#ifndef TEST_DATA_DEF
#define TEST_DATA_DEF

#include "boost/tuple/tuple.hpp"

using namespace bnc_parser;

const static int TEST_DATA_LENGTH = 10;
static TrainingWord TEST_DATA[TEST_DATA_LENGTH] = {
  TrainingWord("Oxford",  "NP0", "NP0" ),
  TrainingWord("city",  "NP1", "NP1" ),
  TrainingWord("council",  "NP0", "NP0" ),
  TrainingWord(".", "PUN", "PUN" ),
  TrainingWord("Health",  "NN1", "NN1" ),
  TrainingWord("and",  "CJC", "CJC" ),
  TrainingWord("environmental", "AJ0", "AJ0" ),
  TrainingWord("protection",  "NN1", "NN1" ),
  TrainingWord("committee",  "NN0", "NN0" ),
  TrainingWord(".",  "PUN", "PUN" )
};

// Brill's rules
/*const static int NUM_RULE_TEMPLATES = 42;
static string RULE_TEMPLATES[NUM_RULE_TEMPLATES] = {
  "TAG[0];",
  "TAG[-1];",
  "TAG[1];",
  "TAG[-2];",
  "TAG[2];",
  "TAG[0]&TAG[1];",
  "TAG[0]&TAG[-1];",
  "TAG[0]&TAG[-1]&TAG[-2];",
  "TAG[0]&TAG[1]&TAG[2];",
  "TAG[0]&TAG[1]&TAG[-1];",
  "TAG[0]&TAG[1][0:2];",
  "TAG[0]&TAG[-1][0:2];",
  "TAG[0]&TAG[-1][0:2]&TAG[-2][0:2];",
  "TAG[0]&TAG[1][0:2]&TAG[2][0:2];",
  "TAG[0]&TAG[1][0:2]&TAG[-1][0:2];",
  "TAG[-2]&TAG[1];",
  "TAG[-2][0:2]&TAG[1][0:2];",
  "TAG[1]&TAG[2];",
  "TAG[1][0:2]&TAG[2][0:2];",
  "TAG[-3]&TAG[-2]&TAG[-1];",
  "TAG[-3][0:2]&TAG[-2][0:2]&TAG[-1][0:2];",
  "TAG[1]&TAG[2]&TAG[3];",
  "TAG[1][0:2]&TAG[2][0:2]&TAG[3][0:2];",
  "TAG[-1]&TAG[1];",
  "TAG[-1][0:2]&TAG[1][0:2];",
  "TAG[-2]&TAG[-1];",
  "TAG[-2][0:2]&TAG[-1][0:2];",
  "TAG[1]&TAG[2];",
  "TAG[1][0:2]&TAG[2][0:2];",
  "WRD[-1];",
  "WRD[1];",
  "WRD[-2];",
  "WRD[2];",
  "WRD[-2]&WRD[-1];",
  "WRD[1]&WRD[2];",
  "WRD[-1]&WRD[0];",
  "WRD[0]&WRD[1];",
  "WRD[0];",
  "WRD[-1]&TAG[-1];",
  "WRD[1]&TAG[1];",
  "WRD[0]&WRD[-1]&TAG[-1];",
  "WRD[0]&WRD[1]&TAG[1];"
};*/

// Allan's rules
const static int NUM_RULE_TEMPLATES = 10;
static string RULE_TEMPLATES[NUM_RULE_TEMPLATES] = {
  "TAG[0];",
  "TAG[0]&TAG[1];",
  "TAG[0]&TAG[-1];",
  "TAG[0]&TAG[-1]&TAG[-2];",
  "TAG[0]&TAG[1]&TAG[2];",
  "TAG[0]&TAG[1]&TAG[-1];",
  "WRD[0];",
  "WRD[0]&TAG[-1];",
  "WRD[0]&TAG[1];",
  "WRD[0]&TAG[-1]&TAG[1];"
};

// Easy to reason about rules
/*const static int NUM_RULE_TEMPLATES = 2;
static string RULE_TEMPLATES[NUM_RULE_TEMPLATES] = {
  "TAG[0];",
  "TAG[-1];"
};*/

struct testfile {
  TrainingWord *content;
  int len;
  string name;
};

const static int TEST_FILES_NUM = 2;
static struct testfile TEST_FILES[TEST_FILES_NUM] = { { TEST_DATA, 4, "file1.xml" }, { TEST_DATA + 4, 6, "file2.xml" } };

#endif