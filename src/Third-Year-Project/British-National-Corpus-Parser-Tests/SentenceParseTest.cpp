#include <boost/test/unit_test.hpp>
#include <boost/bind.hpp>

#include "stdafx.h"
#include "TestData.h"

using namespace bnc_parser;

void writeToFile(string filename, TrainingWord *data, int len) {
  ofstream file(filename);
  file << "<p><a name=\"000\">\n";
  for (int i = 0; i < len; i++) {
    file << data[i].getTag() << "!!" << data[i].getWord() << "\n";
  }
  file.close();
}

BOOST_AUTO_TEST_CASE(TestParseSentence)
{
  cout << "Starting sentence parsing test...\n";
  string root = "C:\\temp\\BncTest\\";
  cout << "Creating corpus on disk...";
  // Create blank files
  for (int i = 0; i < TEST_FILES_NUM; i++) {
    ofstream file;
    writeToFile(root + TEST_FILES[i].name, TEST_FILES[i].content, TEST_FILES[i].len);
    file.close();
  }
  cout << " done\n";
  cout << "Parsing and validating corpus...";
  concurrent_unordered_set<char*, CharHasher, CharEquals> tags, words;
  bnc_parser::BncIterator parser(root, &tags, &words, false);
  int i = 0;
  while (!parser.isEmpty()) {
    vector<bnc_parser::TrainingWord>* sentence = *parser;
    for (unsigned int j = 0; j < sentence->size(); j++) {
      bnc_parser::TrainingWord w = (*sentence)[j];
      BOOST_REQUIRE_EQUAL(w.getWord() , TEST_DATA[i].getWord());
      BOOST_REQUIRE_EQUAL(w.getTag(), TEST_DATA[i].getTag());
      i++;
    }
    parser++;
  }
  cout << " done\n";
  for (char* c : tags) free(c);
  for (char* c : words) free(c);
  BOOST_REQUIRE_EQUAL(i, 10);
}