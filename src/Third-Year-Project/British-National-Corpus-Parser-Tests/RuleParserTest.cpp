#include <boost/test/unit_test.hpp>
#include <boost/bind.hpp>
#include <map>

#include "stdafx.h"
#include "TestData.h"
#include "RuleSchema.h"

using namespace brill_tagger;

BOOST_AUTO_TEST_CASE(TestRuleParser)
{
  cout << "Starting rule parser test\n";
  map<string, string> testData;
  testData["TAG[0]&TAG[1][0:1];"] = "(TAG[0]=''&TAG[1][0:1]='')->''";
  testData["WRD[0];"] = "(WRD[0]='')->''";
  testData["WRD[-3]&WRD[1][0:1]&TAG[1];"] = "(WRD[-3]=''&WRD[1][0:1]=''&TAG[1]='')->''";
  testData["WRD[0][0:3];"] = "(WRD[0][0:3]='')->''";
  Parser p;
  cout << "Test data generated... parsing rules...";
  for (auto pair : testData) {
    Rule *r = p.parse(pair.first);
    BOOST_REQUIRE_EQUAL(r->toString(), pair.second);
    delete r;
  }
  cout << "Done\n";
}

BOOST_AUTO_TEST_CASE(TestRuleFileParser)
{
  cout << "Starting rule file parser test...\n";
  string path = "C:\\temp\\BrillRulesTest.tmpl";
  cout << "Writing sample rules...\n";
  // Write to a rule file
	ofstream outputFile(path);
  for (int i = 0; i < NUM_RULE_TEMPLATES; i++) {
    outputFile << RULE_TEMPLATES[i] << "\n";
  }
	outputFile.close();
  cout << "Parsing rules";
  // Read from the file
  Parser p;
  vector<Rule*> rules = p.parseTemplateFile(path);
  // Check the output is the same
  BOOST_REQUIRE_EQUAL(rules.size(), NUM_RULE_TEMPLATES);
  cout << "... done\n";
  for (Rule* r : rules) {
    delete r;
  }
}