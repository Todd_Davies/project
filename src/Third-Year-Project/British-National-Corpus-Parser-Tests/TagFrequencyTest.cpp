#include <boost/test/unit_test.hpp>
#include <boost/bind.hpp>

#include "stdafx.h"
#include "TestData.h"

using namespace bnc_parser;
using namespace tag_frequency;

BOOST_AUTO_TEST_CASE(TestFrequencyCount)
{
  TagFrequency frequency;
  for (int i = 0; i < TEST_DATA_LENGTH; i++) {
    frequency.add(TEST_DATA[i]);
  }
  
  for (int i = 0; i < 10; i++) {
    TrainingWord word = TEST_DATA[i];
    BOOST_REQUIRE_EQUAL(word.getTag(), frequency.getMostCommonTag(word.getWord()));
  }
}