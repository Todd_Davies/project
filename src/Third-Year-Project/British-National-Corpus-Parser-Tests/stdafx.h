// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#define BOOST_TEST_MODULE POSTagTests

#include <boost/test/unit_test.hpp>
#include "BncParser.h"
#include "TagFrequency.h"


// TODO: reference additional headers your program requires here
