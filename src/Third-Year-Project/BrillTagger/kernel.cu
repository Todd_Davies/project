
#include "kernel.cuh"

using namespace tag_frequency;
using namespace bnc_parser;
using namespace brill_tagger;
using namespace std;

typedef struct { const char* word; char* tag; char* correctTag; } WordTag;
typedef struct { int prerequisites; bool *type; int *offset; char** values; const char* to; } CudaRule;

__global__ void printKernel(WordTag** corpus, int *lengths, size_t size)
{
  int sentenceNum = threadIdx.x;
  for (int i = 0; i < lengths[sentenceNum]; i++) {
    printf("Sentence: %d, Word: %s, Tag: %s\n", sentenceNum, corpus[sentenceNum][i].word, corpus[sentenceNum][i].tag);
  }
}

__global__ void printSentenceKernel(WordTag* sentence)
{
  int wordNum = threadIdx.x;
  printf("Word: %s, Tag: %s\n", sentence[wordNum].word, sentence[wordNum].tag);
}

__device__ int stringCompare(const char* str1, const char* str2) {
  while (str1 != NULL && str2 != NULL) {
    if (*str1 != *str2) return -1;
    str1++;
    str2++;
  }
  return (str1 == NULL && str2 == NULL) ? 0 : -1;
}

__global__ void scoreKernel(WordTag** corpus, int *lengths, size_t numSentences, CudaRule* rules, int* netScores, int* grossScores, size_t numRules) {
  // Get the rule we're working with
  int ruleNumber = threadIdx.x + blockIdx.x * blockDim.x;
  if (ruleNumber >= numRules) return;
  CudaRule rule = rules[ruleNumber];
  // Keep the scores
  int net = 0, gross = 0;
  // Iterate over the corpus
  for (size_t s = 0; s < numSentences; s++) {
    for (size_t w = 0; w < lengths[s]; w++) {
      // Does apply
      for (int prerequisite = 0; prerequisite < rule.prerequisites; prerequisite++) {
        int offset = rule.offset[prerequisite] + w;
        if (offset < 0 || offset > lengths[s]) continue;
        if (stringCompare(rule.values[prerequisite], rule.type[prerequisite] ? corpus[s][w].word : corpus[s][w].tag) != 0) continue;
      }
      // gross++
      gross++;
      // Decrement net if we already have the tag
      if (stringCompare(rule.to, corpus[s][w].tag) == 0) net--;
      // Increment net if we're going to the right tag
      if (stringCompare(rule.to, corpus[s][w].correctTag) == 0) net++;
    }
  }
  // Write the net and gross
  netScores[ruleNumber] = net;
  grossScores[ruleNumber] = gross;
}


WordTag* toWordTagArray(vector<TrainingWord> *trainingWords) {
  WordTag *out = new WordTag[trainingWords->size()];
  for (unsigned int i = 0; i < trainingWords->size(); i++) {
    const char* word = trainingWords->at(i).getWordChars();
    const char* tag = trainingWords->at(i).getTrainingTagChars();
    const char* desiredTag = trainingWords->at(i).getTagChars();
    char* t = new char[strlen(tag)];
    strcpy(t, tag);
    char* dt = new char[strlen(desiredTag)];
    strcpy(dt, desiredTag);
    out[i] = { word, t, dt };
  }
  return out;
}

WordTag* constructDeviceSentence(vector<TrainingWord> *trainingWords, int *length) {
  WordTag *h_corpus = toWordTagArray(trainingWords);
  WordTag *d_corpus = nullptr;
  cudaMalloc(&d_corpus, sizeof(WordTag) * trainingWords->size());
  for (size_t i = 0; i < trainingWords->size(); i++) {
    char* d_word;
    char* d_tag;
    size_t wordSize = sizeof(char) * strlen(h_corpus[i].word);
    size_t tagSize = sizeof(char) * strlen(h_corpus[i].tag);
    cudaMalloc(&d_word, wordSize);
    cudaMalloc(&d_tag, tagSize);
    cudaMemcpy(d_word, h_corpus[i].word, wordSize, cudaMemcpyHostToDevice);
    cudaMemcpy(d_tag, h_corpus[i].tag, tagSize, cudaMemcpyHostToDevice);
    h_corpus[i].word = d_word;
    h_corpus[i].tag = d_tag;
  }
  cudaMemcpy(d_corpus, h_corpus, sizeof(WordTag) * trainingWords->size(), cudaMemcpyHostToDevice);
  *length = trainingWords->size();
  delete h_corpus;
  return d_corpus;
}

WordTag** constructDeviceCorpus(size_t sentences, BncIterator *bnc, int **lengths) {
  WordTag** h_corpus = new WordTag*[sentences];
  int *h_lengths = new int[sentences];
  for (size_t i = 0; i < sentences; i++, (*bnc)++) {
    h_corpus[i] = constructDeviceSentence((**bnc), &(h_lengths[i]));
  }
  WordTag** d_corpus = nullptr;
  cudaMalloc(&d_corpus, sizeof(WordTag*) * sentences);
  cudaMemcpy(d_corpus, h_corpus, sizeof(WordTag*) * sentences, cudaMemcpyHostToDevice);
  int* d_lengths = nullptr;
  cudaMalloc(&d_lengths, sizeof(int) * sentences);
  cudaMemcpy(d_lengths, h_lengths, sizeof(int) * sentences, cudaMemcpyHostToDevice);
  *lengths = d_lengths;
  delete h_corpus;
  delete h_lengths;
  return d_corpus;
}

CudaRule* toRuleArray(vector<Rule*> *rules) {
  CudaRule *out = new CudaRule[rules->size()];
  for (unsigned int i = 0; i < rules->size(); i++) {
    Rule *r = (*rules)[i];
    out[i].prerequisites = r->prerequisites.size();
    out[i].type = new bool[out[i].prerequisites];
    out[i].offset = new int[out[i].prerequisites];
    out[i].values = new char*[out[i].prerequisites];
    out[i].to = r->toTag;
    for (int j = 0; j < out[i].prerequisites; j++) {
      RulePrerequisite *rp = r->prerequisites.at(j);
      out[i].type[j] = rp->isWordType();
      out[i].offset[j] = rp->getRelativeIndex();
      out[i].values[j] = rp->getValue();
    }
  }
  return out;
}

CudaRule* constructDeviceRules(vector<Rule*> *rules) {
  // Convert rues
  CudaRule* h_rules = toRuleArray(rules);
  // Create structure on GPU
  CudaRule* d_rules = nullptr;
  cudaMalloc(&d_rules, sizeof(CudaRule) * rules->size());
  // Copy internal pointers
  for (unsigned int i = 0; i < rules->size(); i++) {
    bool* d_type = nullptr;
    int* d_offset = nullptr;
    char** d_values = nullptr;
    char *d_to = nullptr;
    // Put the types on the GPU
    cudaMalloc(&d_type, sizeof(bool) * rules->size());
    cudaMemcpy(d_type, h_rules[i].type, sizeof(bool) * h_rules[i].prerequisites, cudaMemcpyHostToDevice);
    delete h_rules[i].type;
    // Put the offsets on the GPU
    cudaMalloc(&d_offset, sizeof(int) * rules->size());
    cudaMemcpy(d_offset, h_rules[i].offset, sizeof(int) * h_rules[i].prerequisites, cudaMemcpyHostToDevice);
    delete h_rules[i].offset;
    // Put the prerequisite values on the GPU
    for (int j = 0; j < h_rules[i].prerequisites; j++) {
      char* d_value = nullptr;
      if (h_rules[i].values[j] != NULL) {
        int size = sizeof(char) * strlen(h_rules[i].values[j]);
        cudaMalloc(&d_value, size);
        cudaMemcpy(d_value, h_rules[i].values[j], size, cudaMemcpyHostToDevice);
      }
      h_rules[i].values[j] = d_value;
    }
    cudaMalloc(&d_values, sizeof(char*) * rules->size());
    cudaMemcpy(d_values, h_rules[i].values, sizeof(char*) * h_rules[i].prerequisites, cudaMemcpyHostToDevice);
    delete h_rules[i].values;
    // Put the to value on the GPU
    if (h_rules[i].to != NULL) {
      cudaMalloc(&d_to, sizeof(char) * strlen(h_rules[i].to));
      cudaMemcpy(d_to, h_rules[i].to, sizeof(char) * strlen(h_rules[i].to), cudaMemcpyHostToDevice);
    }
    // Don't delete the to value since it's probably in the big map 
    h_rules[i].offset = d_offset;
    h_rules[i].type = d_type;
    h_rules[i].to = d_to;
    h_rules[i].values = d_values;
  }
  // Move structure to GPU
  cudaMemcpy(d_rules, h_rules, sizeof(h_rules), cudaMemcpyHostToDevice);
  // Delete host rules
  delete h_rules;
  return d_rules;
}

int NUM_BASE_TRAIN = 1000;
int NUM_BRILL_TRAIN = 30;
int NUM_BRILL_RULES = 3;

int main()
{
  // Keep track of the tags and words we've seen
  concurrent_unordered_set<char*, CharHasher, CharEquals> tags, words;
  // Create a BNC iterator
  BncIterator bnc = BncIterator("C:\\Users\\Todd\\dev\\project\\data\\BNC\\BNC", &tags, &words);
  // Train the base tagger
  TagFrequency base;
  for (int i = 0; i < NUM_BASE_TRAIN && !bnc.isEmpty();) {
    for (TrainingWord t : **bnc) base.add(t);
    bnc++;
    i++;
    if (i % 1000 == 0) cout << "\rTraining base tagger: " << (100.0 / NUM_BASE_TRAIN) * i << "% ";
  }
  // Load the rule templates
  cout << "\nParsing rule templates... ";
  Parser p;
  vector<Rule*> ruleTemplates = p.parseTemplateFile("C:\\temp\\BrillRulesTest.tmpl");
  cout << ruleTemplates.size() << " templates parsed\n";
  // Move the rules to the GPU
  CudaRule* d_rules = constructDeviceRules(&ruleTemplates);
  // Move the corpus to the GPU
  //int d_lengths = 0;
  //WordTag *d_sentence = constructDeviceSentence(*bnc, &d_lengths);
  //printSentenceKernel<< <1, d_lengths >> >(d_sentence);
  int *d_lengths = nullptr;
  WordTag** d_corpus = constructDeviceCorpus(100, &bnc, &d_lengths);
  printKernel << <1, 2 >> >(d_corpus, d_lengths, 20);
  cudaDeviceSynchronize();
  // Deallocate the templates
  for (Rule* r : ruleTemplates) delete r;
  return 0;
}