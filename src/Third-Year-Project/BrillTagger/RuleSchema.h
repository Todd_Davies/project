#include "RuleParser.h"
#include <vector>

using namespace std;

namespace brill_tagger {

  /**
   * Initialises and traverses a finite automata which parses Brill rules.
   */
  class Parser {
  private:
    vector<Node*> nodes;

  public:
    // Constructor
    Parser();
    // Destructor
    ~Parser();
    // Parse a single rule string
    Rule* parse(string ruleTemplate);
    // Parse a rule file
	  vector<Rule*> parseTemplateFile(string filename);
  };

}