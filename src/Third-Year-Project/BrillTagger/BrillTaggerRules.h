#ifndef BRILL_RULES
#define BRILL_RULES

#include <boost/lexical_cast.hpp>

#include <concurrent_unordered_set.h>
#include <stdlib.h>
#include "BncParser.h"
#include <vector>
#include <sstream>

using namespace std;
using namespace concurrency;
using namespace bnc_parser;

namespace brill_tagger {

  class RulePrerequisite {
    // Does this prerequisite operate on tags or words?
    // TODO: Expand this to an enum later to let rules be more expressive
    bool wordType = false;
    // The relative index of the entity to inspect
    short relativeIndex = 0;
    // How far to match the tag (0 == whole tag)
    short matchLen = 0;
    // The value of the index
    char* value = nullptr;
  public:
    RulePrerequisite();
    RulePrerequisite(bool wordType, int relativeIndex, int matchLen, char* val);
    // Instansiate this rule prerequisite with a value
    RulePrerequisite* instantiate(vector<TrainingWord>* context, int index, concurrent_unordered_set<char*, CharHasher, CharEquals> *tags, concurrent_unordered_set<char*, CharHasher, CharEquals> *words);
    // Is this prerequisite satisfied by the context?
    bool isSatisfied(vector<TrainingWord>* context, int currentWordIndex);
    // Getter for the relative index
    short getRelativeIndex();
    // Getter for the match length
    short getMatchLength();
    // Getter for the word type
    bool isWordType();
    // Getter for the value
    char* getValue();
    string toString();
    // Setter methods
    RulePrerequisite setWordType(bool wordType);
    RulePrerequisite setRelativeIndex(short relativeIndex);
    RulePrerequisite setMatchLength(short matchLen);
    size_t hash() const;
  };

  class Rule {
    
  private:
    // Cache the hash!
    mutable bool hashed = false;
    mutable size_t cachedHash = 0;

  public:
    Rule();
    ~Rule();
    // The tag that will be used if the rule applies
    char* toTag;
    // The conditions that must be true for the rule to be applied
    vector<RulePrerequisite*> prerequisites;
    // Adds a prerequisite to the rule.
    // Should only be called straight after instansiation.
    void addPrerequisite(RulePrerequisite *pre);
    // Apply the rule to a context. Does nothing if the prerequisites are not satisfied
    vector<TrainingWord>* apply(vector<TrainingWord>* context, int currentWordIndex);
    // Returns true if the rule applies
    bool doesApply(vector<TrainingWord>* context, int currentWordIndex);
    // Returns the to tag;
    char* getTo() const;
    void setTo(char* newTo);
    // Returns the hash of the prerequisites
    size_t hash() const;
    // Instansiate the rule from a context
    Rule* instantiate(vector<TrainingWord>* context, int index, concurrent_unordered_set<char*, CharHasher, CharEquals> *tags, concurrent_unordered_set<char*, CharHasher, CharEquals> *words);
    // Represent the rule as a string
    string toString() const;
    // Equals method
    bool operator==(const Rule &other) const;

  };

}

#endif