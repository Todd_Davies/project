#include "BrillTaggerRules.h"
#include "TagFrequency.h"
#include "kernel.cuh"

#include "boost/tuple/tuple.hpp"

#include <ppl.h>
#include <concurrent_unordered_set.h>
#include <concurrent_vector.h>

#include <vector>
#include <unordered_set>
#include <queue>
#include <chrono>

using namespace tag_frequency;
using namespace concurrency;

namespace brill_tagger {

  // Finds the hash code for a rule for use with hash sets
  struct RuleHasher {
    size_t operator()(const Rule *t) const {
      return t->hash();
    }
  };

  // Rule equality tester
  struct RuleEquals {
    bool operator()(Rule* r1, Rule* r2) const {
      return r1->hash() == r2->hash();
    }
  };

  // Struct for recording the score of a given rule
  struct RuleScore
  {
    Rule* rule;
    int net, gross;
  };

  // So that Rules can be used with a Heap
  // Net is compared first, then gross, then hash code (to ensure a strict ordering)
  struct HeapComperator {
    bool operator()(const RuleScore a, const RuleScore b) const{
      int net = a.net - b.net;
      if (net == 0) {
        int gross = a.gross - b.gross;
        if (gross == 0) {
          return a.rule->hash() < b.rule->hash();
        }
        else return gross < 0;
      }
      else return net < 0;
    }
  };

  typedef vector<vector<TrainingWord>> Corpus;
  typedef concurrent_unordered_set<char*, CharHasher, CharEquals> StringPool;
  typedef concurrent_unordered_set<Rule*, RuleHasher, RuleEquals> RuleSet;

  /**
   * Reads a number of training sentences from the BNC corpus, and uses them to formulate brill rules.
   */
  vector<Rule*> trainRules(vector<Rule*> templates, BncIterator *bnc,
    TagFrequency *baseTagger, StringPool *tags, StringPool *words,
    int numTrainingSentences, size_t numRules, int print, int threshold,
    int* pNumRules);

  /**
  * Reads a number of training sentences from the BNC corpus, and uses them to formulate brill rules.
  * Computed in parallel on the CPU.
  */
  vector<Rule*> trainRulesParallel(vector<Rule*> templates, BncIterator *bnc,
    TagFrequency *baseTagger, StringPool *tags, StringPool *words,
    int numTrainingSentences, size_t numRules, int print, int threshold,
    int* pNumRules);
}