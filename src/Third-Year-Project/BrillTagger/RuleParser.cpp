#include "RuleParser.h"


namespace brill_tagger {

  Node::Node(bool finalNode, void(*m)(RulePrerequisite*, char)) : finalNode(finalNode),
    mutateFunction(m) {}

  void  Node::addEdge(Node *next, int n_args, ...) {
    va_list ap;
    va_start(ap, ++n_args);
    for (int i = 2; i <= n_args; i++) {
      addEdge(next, va_arg(ap, char));
    }
  }

  void  Node::addEdge(Node *next, char c) {
    transition[c] = next;
  }

  Node* Node::traverse(RulePrerequisite* rule, char c) {
    auto item = transition.find(c);
    return item == transition.end() ? nullptr : item->second->mutate(rule, c);
  }

  Node* Node::mutate(RulePrerequisite* rule, char c) {
    mutateFunction(rule, c);
    return this;
  }

}