#include "BrillTaggerTrainer.h"

namespace brill_tagger {

  /**
   * Initialises a corpus of n sentences, where n = numTrainingSentences.
   * Training tags are initially set to the empty string.
   */
  Corpus initTrainingCorpus(BncIterator *bnc, int numTrainingSentences, bool quiet) {
    int words = 0;
    Corpus out;
    while (numTrainingSentences-- > 0 && !bnc->isEmpty()) {
      vector<TrainingWord> *rawSentence = **bnc;
      words += rawSentence->size();
      vector<TrainingWord> trainingSentence;
      for (size_t i = 0; i < rawSentence->size(); i++) {
        TrainingWord word = (*rawSentence)[i];
        trainingSentence.push_back(TrainingWord(word.getWordChars(), nullptr, word.getTagChars()));
      }
      out.push_back(trainingSentence);
      (*bnc)++;
    }
    if (!quiet) cout << words << " read\n";
    return out;
  }

  /**
   * Tag corpus with the base tagger
   */
  void baseTagCorpusSerial(TagFrequency *baseTagger, Corpus *corpus, StringPool *tags) {
    for (size_t j = 0; j < corpus->size(); j++) {
      for (size_t k = 0; k < (*corpus)[j].size(); k++) {
        (*corpus)[j][k].setTrainingTag(baseTagger->getMostCommonTag((*corpus)[j][k].getWord()), tags);
      }
    }
  }

  /**
   * Tag corpus with the base tagger in parallel
   */
  void baseTagCorpusParallel(TagFrequency *baseTagger, Corpus *corpus, StringPool *tags) {
    parallel_for(0, (int) corpus->size(), [&baseTagger, &corpus, &tags](int j) {
      for (size_t k = 0; k < (*corpus)[j].size(); k++) {
        (*corpus)[j][k].setTrainingTag(baseTagger->getMostCommonTag((*corpus)[j][k].getWord()), tags);
      }
    });
  }

  /**
   * Generate rules based on the errors in the existing corpus
   */
  RuleSet* generateSeedRules(vector<Rule*> *templates, Corpus *corpus, StringPool *tags, 
      StringPool *words) {
    RuleSet *seeds = new RuleSet();
    for (size_t j = 0; j < corpus->size(); j++) {
      for (size_t k = 0; k < (*corpus)[j].size(); k++) {
        if (strcmp((*corpus)[j][k].getTrainingTagChars(), (*corpus)[j][k].getTagChars()) != 0) {
          for (Rule *tmpl : *templates) {
            Rule *generatedRule = tmpl->instantiate(&((*corpus)[j]), k, tags, words);
            if (seeds->find(generatedRule) == seeds->end()) {
              seeds->insert(generatedRule);
            }
            else delete generatedRule;
          }
        }
      }
    }
    return seeds;
  }

  RuleSet* generateSeedRulesParallel(vector<Rule*> *templates, Corpus *corpus,
      StringPool *tags, StringPool *words) {
    RuleSet *seeds = new RuleSet();
    parallel_for(0, (int)corpus->size(), [&seeds, &corpus, &templates, &tags, &words](int j) {
      for (size_t k = 0; k < (*corpus)[j].size(); k++) {
        if (strcmp((*corpus)[j][k].getTrainingTagChars(), (*corpus)[j][k].getTagChars()) != 0) {
          for (Rule *tmpl : *templates) {
            Rule *generatedRule = tmpl->instantiate(&((*corpus)[j]), k, tags, words);
            if (seeds->find(generatedRule) == seeds->end()) {
              seeds->insert(generatedRule);
            }
            else delete generatedRule;
          }
        }
      }
    });
    return seeds;
  }

  void scoreRules(vector<RuleScore> *heap, RuleSet *candidates, Corpus *corpus) {
    // Score each rule & add it to the heap
    for (Rule *candidate : *candidates) {
      int net = 0, gross = 0;
      for (size_t j = 0; j < corpus->size(); j++) {
        for (size_t k = 0; k < (*corpus)[j].size(); k++) {
          if (candidate->doesApply(&((*corpus)[j]), k)) {
            gross++;
            const char* desiredTag = (*corpus)[j][k].getTagChars();
            // Decrement the net is we already have the desired tag
            if (strcmp((*corpus)[j][k].getTrainingTagChars(), desiredTag) == 0) {
              net--;
            }
            // Increment it if we're going to the desired tag
            if (strcmp(candidate->getTo(), desiredTag) == 0) {
              net++;
            }
          }
        }
      }
      heap->push_back({ candidate, net, gross });
      std::push_heap(heap->begin(), heap->end(), HeapComperator());
    }
  }

  concurrent_vector<RuleScore>* scoreRulesParallel(RuleSet *candidates, Corpus *corpus) {
    concurrent_vector<RuleScore> *rules = new concurrent_vector<RuleScore>();
    // Score each rule & add it to the heap
    parallel_for_each(candidates->begin(), candidates->end(), [&corpus, &rules](Rule* candidate) {
      int net = 0, gross = 0;
      for (size_t j = 0; j < corpus->size(); j++) {
        for (size_t k = 0; k < (*corpus)[j].size(); k++) {
          if (candidate->doesApply(&((*corpus)[j]), k)) {
            gross++;
            const char* desiredTag = (*corpus)[j][k].getTagChars();
            // Decrement the net is we already have the desired tag
            if (strcmp((*corpus)[j][k].getTrainingTagChars(), desiredTag) == 0) {
              net--;
            }
            // Increment it if we're going to the desired tag
            if (strcmp(candidate->getTo(), desiredTag) == 0) {
              net++;
            }
          }
        }
      }
      rules->push_back({ candidate, net, gross });
    });
    return rules;
  }

  void applyAndGenerate(Rule *rule, vector<Rule*> *templates, RuleSet *candidates,
      Corpus *corpus, StringPool *tags, StringPool *words) {
    for (size_t j = 0; j < corpus->size(); j++) {
      for (size_t k = 0; k < (*corpus)[j].size(); k++) {
        if (rule->doesApply(&((*corpus)[j]), k)) {
          rule->apply(&((*corpus)[j]), k);
          // If this rule is going to mess things up
          if (strcmp((*corpus)[j][k].getTagChars(), rule->getTo()) != 0) {
            // Generate rules to mitigate the mess up
            for (Rule *tmpl : *templates) {
              Rule *generatedRule = tmpl->instantiate(&((*corpus)[j]), k, tags, words);
              if (candidates->find(generatedRule) == candidates->end()) {
                candidates->insert(generatedRule);
              }
              else delete generatedRule;
            }
          }
        }
      }
    }
  }

  void applyAndGenerateParallel(Rule *rule, vector<Rule*> *templates, RuleSet *candidates, 
      Corpus *corpus,  StringPool *tags, StringPool *words) {
    parallel_for(0, (int) corpus->size(), [&corpus, &rule, &candidates, &tags, &words, &templates](int j) {
      for (size_t k = 0; k < (*corpus)[j].size(); k++) {
        if (rule->doesApply(&((*corpus)[j]), k)) {
          rule->apply(&((*corpus)[j]), k);
          // If this rule is going to mess things up
          //if (strcmp((*corpus)[j][k].getTagChars(), rule->getTo()) != 0) {
          for (int i = -2; i <= 2; i++) {
            if (k + i >= 0 || k + i < corpus->at(j).size()) {
              for (Rule *tmpl : *templates) {
                Rule *generatedRule = tmpl->instantiate(&((*corpus)[j]), k, tags, words);
                if (candidates->find(generatedRule) == candidates->end()) {
                  candidates->insert(generatedRule);
                }
                else delete generatedRule;
              }
            }
          }
          //}
        }
      }
    });
  }

  vector<Rule*> trainRules(vector<Rule*> templates, BncIterator *bnc,
      TagFrequency *baseTagger, StringPool *tags, StringPool *words,
      int numTrainingSentences, size_t numRules, int print, int threshold, int* pNumRules) {
    // Base tag the corpus
    if (print == 1) cout << "Reading " << numTrainingSentences << " sentences for training " << numRules << " Brill rules...\n";
    Corpus corpus = initTrainingCorpus(bnc, numTrainingSentences, !print);

    // Initialise the corpus with the base tagger
    if (print == 1) cout << "Tagging the corpus with the base tagger...\n";
    baseTagCorpusSerial(baseTagger, &corpus, tags);

    // Generate our initial rule set
    if (print == 1) cout << "Generating initial rules... ";
    RuleSet *candidates = generateSeedRules(&templates, &corpus, tags, words);
    if (print == 1) cout << " done.\n";
    
    // Output vector
    vector<Rule*> output;

    // Make a heap to put the rules in while we score them
    vector<RuleScore> heap;
    std::make_heap(heap.begin(), heap.end(), HeapComperator());

    // While we still have candidates and haven't generated enough rules
    while (candidates->size() > 0 && output.size() < numRules) {
      if (print == 1) cout << numRules - output.size() << " rules left to find...\n";
      if (print == 1) cout << "Scoring " << candidates->size() << " rules...\n";

      auto start = std::chrono::high_resolution_clock::now();
      // Score the rules, inserting into the heap as we go
      scoreRules(&heap, candidates, &corpus);
      auto finish = std::chrono::high_resolution_clock::now();
      auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
      if (print != 0 && print <= 2) std::cout << "TIME: " << ms.count() << "ms\n";
      if (print != 0 && print <= 2) std::cout << "NUM_RULES: " << candidates->size() << "\n";
      (*pNumRules) += candidates->size();
      // Clear out the candidates now we've added them to the heap
      candidates->clear();

      // Check that the net is greater than 0 (i.e. there was at least one positive rule)
      if (!heap.empty() && heap.front().net > 0) {
        // Pop the top item from the heap
        std::pop_heap(heap.begin(), heap.end(), HeapComperator());
        RuleScore max = heap.back();
        heap.pop_back();
        Rule *topRule = max.rule;

        // Apply the rule
        if (print == 1) cout << "Best score has net of " << max.net << ", applying the rule...\n";
        if (print == 1) cout << "Best rule is " << topRule->toString() << "\n";
        applyAndGenerate(topRule, &templates, candidates, &corpus, tags, words);

        // Add the top rule
        output.push_back(topRule);

        // Don't care about the rest now
        break;

        int deleted = 0;

        // Iterate over the other rules in order of descending score
        if (print == 1) cout << "Pruning other rules...\n";
        while (heap.size()) {
          std::pop_heap(heap.begin(), heap.end(), HeapComperator());
          RuleScore score = heap.back();
          heap.pop_back();
          // Delete if it's a naf rule
          if (score.net < threshold) {
            delete score.rule;
            deleted++;
          }
          // Keep if the rule is okay
          else candidates->insert(score.rule);
        }
      }
      else {
        // No good rules; return what we have
        break;
      }
    }
    // Own accuracy hehehe
    int total = 0;
    int num = 0;
    for (size_t i = 0; i < corpus.size(); i++) {
      for (size_t j = 0; j < corpus.at(i).size(); j++) {
        num++;
        if (strcmp(corpus.at(i).at(j).getTagChars(), corpus.at(i).at(j).getTrainingTagChars()) == 0) total++;
      }
    }
    if (print == 1) cout << (100.0 / num) * total << "\n";

    // Delete the unused candidates
    for (Rule *r : *candidates) delete r;
    free(candidates);
    return output;
  }

  vector<Rule*> trainRulesParallel(vector<Rule*> templates, BncIterator *bnc,
      TagFrequency *baseTagger, StringPool *tags, StringPool *words,
      int numTrainingSentences, size_t numRules, int print, int threshold, int* pNumRules) {
    // Base tag the corpus
    if (print == 1) cout << "Reading " << numTrainingSentences << " sentences for training " << numRules << " Brill rules...\n";
    Corpus corpus = initTrainingCorpus(bnc, numTrainingSentences, !print);

    // Initialise the corpus with the base tagger
    if (print == 1) cout << "Tagging the corpus with the base tagger...\n";
    baseTagCorpusParallel(baseTagger, &corpus, tags);

    // Generate our initial rule set
    if (print == 1) cout << "Generating initial rules...\n";
    RuleSet *candidates = generateSeedRulesParallel(&templates, &corpus, tags, words);

    // Output vector
    vector<Rule*> output;

    // Make a heap to put the rules in while we score them
    vector<RuleScore> heap;
    std::make_heap(heap.begin(), heap.end(), HeapComperator());

    // While we still have candidates and haven't generated enough rules
    while (candidates->size() > 0 && output.size() < numRules) {
      if (print == 1) cout << numRules - output.size() << " rules left to find...\n";
      if (print == 1) cout << "Scoring " << candidates->size() << " rules...\n";

      auto start = std::chrono::high_resolution_clock::now();
      // Score the rules, inserting into the heap as we go
      concurrent_vector<RuleScore> *scores = scoreRulesParallel(candidates, &corpus);
      auto finish = std::chrono::high_resolution_clock::now();
      auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
      if (print != 0 && print <= 2) std::cout << "TIME: " << ms.count() << "ms\n";
      if (print != 0 && print <= 2) std::cout << "NUM_RULES: " << candidates->size() << "\n";
      (*pNumRules) += candidates->size();

      // Add the score to the heap
      for (RuleScore score : *scores) {
        heap.push_back(score);
        std::push_heap(heap.begin(), heap.end(), HeapComperator());
      }

      // Clear out the candidates now we've added them to the heap
      candidates->clear();

      // Check that the net is greater than 0 (i.e. there was at least one positive rule)
      if (heap.size() > 0 && heap.front().net > 0) {
        // Pop the top item from the heap
        std::pop_heap(heap.begin(), heap.end(), HeapComperator());
        RuleScore max = heap.back();
        heap.pop_back();
        Rule *topRule = max.rule;

        // Apply the rule
        if (print == 1) cout << "Best score has net of " << max.net << ", applying the rule...\n";
        if (print == 1) cout << "Best rule is " << topRule->toString() << "\n";
        applyAndGenerateParallel(topRule, &templates, candidates, &corpus, tags, words);

        // Add the top rule
        output.push_back(topRule);

        // Don't care about the rest now
        break;

        // Iterate over the other rules in order of descending score
        if (print == 1) cout << "Pruning other rules...\n";
        while (heap.size()) {
          std::pop_heap(heap.begin(), heap.end(), HeapComperator());
          RuleScore score = heap.back();
          heap.pop_back();
          if (score.net < threshold) {
            delete score.rule;
          }
          else {
            candidates->insert(score.rule);
          }
        }
      }
      else {
        // No good rules, return what we have
        break;
      }
    }

    // Delete the unused candidates
    for (Rule *r : *candidates) delete r;
    free(candidates);

    // Own accuracy hehehe
    int total = 0;
    int num = 0;
    for (size_t i = 0; i < corpus.size(); i++) {
      for (size_t j = 0; j < corpus.at(i).size(); j++) {
        num++;
        if (strcmp(corpus.at(i).at(j).getTagChars(), corpus.at(i).at(j).getTrainingTagChars()) == 0) total++;
      }
    }
    if (print == 1) cout << (100.0 / num) * total << "\n";

    return output;
  }
}