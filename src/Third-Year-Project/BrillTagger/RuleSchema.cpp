#include "RuleSchema.h"

namespace brill_tagger {

  // Mutator function for when nothing should be done
  void(*doNothing)(RulePrerequisite*, char) =
    [](RulePrerequisite *r, char c) { return; };

  // Construct the finite automata
  Parser::Parser() {
    Node *root = new Node(false, doNothing);
    nodes.push_back(root);
    // TAG
    Node *t = new Node(false, doNothing);
    nodes.push_back(t);
    Node *a = new Node(false, doNothing);
    nodes.push_back(a);
    Node *g = new Node(false,
      [](RulePrerequisite *r, char c) { r->setWordType(false); });
    nodes.push_back(g);
    root->addEdge(t, 'T');
    t->addEdge(a, 'A');
    a->addEdge(g, 'G');
    // WRD
    Node *w = new Node(false, doNothing);
    nodes.push_back(w);
    Node *r = new Node(false, doNothing);
    nodes.push_back(r);
    Node *d = new Node(false,
      [](RulePrerequisite *r, char c) { r->setWordType(true); });
    nodes.push_back(d);
    root->addEdge(w, 'W');
    w->addEdge(r, 'R');
    r->addEdge(d, 'D');
    // [
    Node *rBrace = new Node(false, doNothing);
    nodes.push_back(rBrace);
    d->addEdge(rBrace, '[');
    g->addEdge(rBrace, '[');
    // Minus
    Node *minus = new Node(false,
      [](RulePrerequisite *r, char c) {r->setRelativeIndex(-1); });
    nodes.push_back(minus);
    // First number
    Node *num1 = new Node(false,
      [](RulePrerequisite *r, char c) {
        r->setRelativeIndex((r->getRelativeIndex() < 0 ? -1 : 1) * (c - '0'));
      });
    nodes.push_back(num1);
    rBrace->addEdge(num1, 10, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    rBrace->addEdge(minus, '-');
    minus->addEdge(num1, 10, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    // ]
    Node *lBrace = new Node(false, doNothing);
    nodes.push_back(lBrace);
    num1->addEdge(lBrace, ']');
    // Second [
    Node *rBrace2 = new Node(false, doNothing);
    nodes.push_back(rBrace2);
    // Second minus
    Node *minus2 = new Node(false, doNothing);
    nodes.push_back(minus2);
    // Second number (always treat as zero)
    Node *num2 = new Node(false, doNothing);
    nodes.push_back(num2);
    rBrace2->addEdge(num2, 10, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    rBrace2->addEdge(minus2, '-');
    minus2->addEdge(num2, 10, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    // Colon between num 2 and 3
    Node *colon = new Node(false, doNothing);
    nodes.push_back(colon);
    num2->addEdge(colon, ':');
    // Third number
    Node *num3 = new Node(false,
      [](RulePrerequisite *r, char c) { r->setMatchLength(c - '0'); });
    nodes.push_back(num3);
    colon->addEdge(num3, 10, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    num3->addEdge(lBrace, ']');
    // &
    Node *and = new Node(true, doNothing);
    nodes.push_back(and);
    lBrace->addEdge(and, '&');
    and->addEdge(t, 'T');
    and->addEdge(w, 'W');
    // ;
    Node *semicolon = new Node(true, doNothing);
    nodes.push_back(semicolon);
    lBrace->addEdge(semicolon, ';');
    lBrace->addEdge(rBrace2, '[');
  }

  Rule* Parser::parse(string ruleTemplate) {
    // Ignore the emptry string
    if (ruleTemplate.compare("") == 0) return nullptr;
    // Start of the automaton
    Node *n = nodes[0];
    // The current rule prerequisite we're building
    RulePrerequisite *pre = new RulePrerequisite();
    // The output rule
    Rule *out = new Rule();
    // For each character in the input
    for (size_t i = 0; i < ruleTemplate.size(); i++) {
      // If we're at a final node in the automaton, start a new
      // prerequisite
      if (n->finalNode) {
        out->addPrerequisite(pre);
        pre = new RulePrerequisite();
      }
      // Otherwise, traverse to the next node
      Node *next = n->traverse(pre, ruleTemplate.at(i));
      // If the next node is null and we weren't at a final node
      // The parsing failed
      if (next == nullptr && !n->finalNode) {
        out->~Rule();
        delete pre;
        return nullptr;
      } else n = next;
    }
    // Add the last prerequisite
    out->addPrerequisite(pre);
    // Return the rule
    return out;
  }

  vector<Rule*> Parser::parseTemplateFile(string filename) {
	  ifstream openFile = ifstream();
	  openFile.open(filename);
	  vector<Rule*> out;
	  while (!openFile.eof()) {
		  string line = "";
		  getline(openFile, line);
      Rule *rule = parse(line);
		  if (rule != nullptr) out.push_back(rule);
	  }
	  openFile.close();
	  return out;
  }


  Parser::~Parser() {
    // Delete each node in the list of nodes
    for (Node* n : nodes) {
      delete n;
    }
    nodes.empty();
  }
}