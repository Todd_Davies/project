#include "BrillTaggerRules.h"

namespace brill_tagger {

  string WORD = "WRD";
  string TAG = "TAG";

  //==================
  // RulePrerequisite
  //==================

  RulePrerequisite::RulePrerequisite() {}
  RulePrerequisite::RulePrerequisite(bool wordType, int relativeIndex, int matchLen, char* val) : 
      wordType(wordType), relativeIndex(relativeIndex), matchLen(matchLen), value(val) {}

  RulePrerequisite* RulePrerequisite::instantiate(vector<TrainingWord>* context, int index,
      concurrent_unordered_set<char*, CharHasher, CharEquals> *tags, concurrent_unordered_set<char*, CharHasher, CharEquals> *words) {
    size_t thisIndex = relativeIndex + index;
    char* value;
    if (thisIndex < 0 || thisIndex >= context->size()) {
      if (thisIndex < 0) {
        value = new char[6] { 'S', 'T', 'A', 'R', 'T', '\0'};
      } else {
        value = new char[4] { 'E', 'N', 'D', '\0' };
      }
      auto ptr = words->find(value);
      if (ptr == words->end()) words->insert(value);
      else {
        delete value;
        value = *ptr;
      }
    } else {
      const char* oldValue = (wordType) ? (*context)[thisIndex].getWordChars() : (*context)[thisIndex].getTrainingTagChars();
      int oldLength = (wordType) ? (*context)[thisIndex].getWord().length() : (*context)[thisIndex].getTrainingTag().length();
      char* newValue = new char[oldLength + 1];
      strcpy(newValue, oldValue);
      concurrent_unordered_set<char*, CharHasher, CharEquals> *set = (wordType) ? words : tags;
      if (set != nullptr) {
        auto it = set->find(newValue);
        if (it != set->end()) {
          delete newValue;
          newValue = *it;
        }
        else {
          set->insert(newValue);
        }
      }
      value = newValue;
    }
    return new RulePrerequisite(wordType, relativeIndex, matchLen, value);
  }

  bool RulePrerequisite::isSatisfied(vector<TrainingWord>* context, int currentWordIndex) {
    size_t inspectionIndex = currentWordIndex + relativeIndex;
    if (inspectionIndex >= 0 && inspectionIndex < context->size()) {
      const char* thisValue = (wordType)
        ? (*context)[inspectionIndex].getWordChars()
        : (*context)[inspectionIndex].getTrainingTagChars();
      return (matchLen > 0)
        ? strncmp(value, thisValue, matchLen) == 0
        : strcmp(value, thisValue) == 0;
    }
    else return false;
  }

  short RulePrerequisite::getRelativeIndex() {
    return relativeIndex;
  }

  short RulePrerequisite::getMatchLength() {
    return matchLen;
  }

  bool RulePrerequisite::isWordType() {
    return wordType;
  }

  char* RulePrerequisite::getValue() {
    return value;
  }

  string RulePrerequisite::toString() {
    ostringstream s;
    s << (wordType ? WORD : TAG) << "[" << relativeIndex << "]";
    if (matchLen > 0) {
      s << "[0:" << matchLen << "]";
    }
    if (value != nullptr) s << "='" << string(value) << "'";
    else s << "=''";
    return s.str();
  }

  RulePrerequisite RulePrerequisite::setWordType(bool wt) {
    wordType = wt;
    return *this;
  }

  RulePrerequisite RulePrerequisite::setRelativeIndex(short ri) {
    relativeIndex = ri;
    return *this;
  }

  RulePrerequisite RulePrerequisite::setMatchLength(short ml) {
    matchLen = ml;
    return *this;
  }

  size_t RulePrerequisite::hash() const {
    size_t result = 7;
    if (value != nullptr) {
      std::hash<class T*> hashFn;
      result = (37 * result) + hashFn((T*)value);
    }
    result = (37 * result) + relativeIndex;
    result = (37 * result) + matchLen;
    result = (37 * result) + ((wordType) ? 0 : 1);
    return result;
  }

  //==================
  // Rule
  //==================

  Rule::Rule() {}

  Rule::~Rule() {
    for (RulePrerequisite *rp : prerequisites) {
      delete rp;
    }
  }

  void Rule::addPrerequisite(RulePrerequisite *pre) {
    prerequisites.push_back(pre);
  }

  vector<TrainingWord>* Rule::apply(vector<TrainingWord>* context, int currentWordIndex) {
    for (RulePrerequisite *prerequisite : prerequisites) {
      if (!prerequisite->isSatisfied(context, currentWordIndex)) return context;
    }
    (*context)[currentWordIndex].setTrainingTag(toTag);
    return context;
  }

  Rule* Rule::instantiate(vector<TrainingWord>* context, int index, concurrent_unordered_set<char*, CharHasher, CharEquals> *tags, concurrent_unordered_set<char*, CharHasher, CharEquals> *words) {
    char* tag = new char[(*context)[index].getTag().length() + 1];
    strcpy(tag, (*context)[index].getTagChars());
    auto elem = tags->find(tag);
    if (elem != tags->end()) {
      delete tag;
      tag = *elem;
    }
    else {
      tags->insert(tag);
    }
    Rule* out = new Rule();
    for (size_t i = 0; i < prerequisites.size(); i++) {
      out->addPrerequisite(prerequisites[i]->instantiate(context, index, tags, words));
    }
    out->setTo(tag);
    return out;
  }

  bool Rule::doesApply(vector<TrainingWord>* context, int currentWordIndex) {
    for (size_t i = 0; i < prerequisites.size(); i++) {
      if (!prerequisites.at(i)->isSatisfied(context, currentWordIndex)) return false;
    }
    return true;
  }

  char* Rule::getTo() const {
    return toTag;
  }

  void Rule::setTo(char* to) {
    toTag = to;
  }

  string Rule::toString() const {
    stringstream s;
    bool first = true;
    s << "(";
    for (RulePrerequisite *part : prerequisites) {
      if (!first) s << "&";
      else first = false;
      s << part->toString();
    }
    if (toTag != nullptr) s << ")->'" << toTag << "'";
    else s << ")->''";
    return s.str();
  }

  bool Rule::operator==(const Rule &other) const {
    return toString().compare(other.toString()) == 0;
  }

  size_t Rule::hash() const {
    if (!hashed) {
      size_t result = 17;
      for (RulePrerequisite *rp : prerequisites) {
        result = (37 * result) + rp->hash();
      }
      std::hash<class T*> hashFn;
      result = (37 * result) + hashFn((T*) toTag);
      cachedHash = result;
      hashed = true;
    }
    return cachedHash;
  }
}