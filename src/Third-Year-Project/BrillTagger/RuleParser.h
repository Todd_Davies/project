#ifndef PARSER_NODE
#define PARSER_NODE

#include <vector>
#include <map>
#include <stdarg.h>
#include "BrillTaggerRules.h"

using namespace std;

namespace brill_tagger {

  /**
   * Used to represent a finite automata which input can be fed through to parse Brill rule templates.
   */
  class Node {
  private:
    map<char, Node*> transition;
    void(*mutateFunction)(RulePrerequisite*, char edge);

  public:
    const bool finalNode;
    Node(bool finalNode, void(*)(RulePrerequisite*, char edge));
    void addEdge(Node *next, int nArgs, ...);
    void addEdge(Node *next, char c);
    Node* traverse(RulePrerequisite* rule, char c);
    Node* mutate(RulePrerequisite *r, char edge);
  };



}
#endif